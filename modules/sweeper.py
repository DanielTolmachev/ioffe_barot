#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Daniel
#
# Created:     13.03.2015
# Copyright:   (c) Daniel 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from __future__ import print_function
import time

class Sweeper(object):
    HWClass = None
    _setfunc = None
    _queryfunc = None
    stopped = False
    resume = False
    loop = True
    bidirectional = False
    value = 0
    reverse = False
    loops = 0
    maxloops = 0 #0 for continued looping
    def __init__(self,HWclass = None,
        setfunc = None,
        queryfunc = None,
        verbose = False):
        self.HWClass = HWclass
        if HWclass:
            if hasattr(HWclass,'set'):
                self._setfunc = HWclass.set
            if hasattr(HWclass,'query'):
                self._setfunc = HWclass.query
        if setfunc:
            self._setfunc = setfunc
        if queryfunc:
            self._queryfunc = queryfunc
        if not self._setfunc: #use stub if not set
            self._setfunc = self._set_stub
        if not self._queryfunc:
            self._queryfunc = self._query_stub
        self.verbose = verbose
        if verbose:
            print('setfunc was set to', self._setfunc)
            print('queryfunc was set to', self._queryfunc)
            print(self.queryfunc)
    def _query_stub(self):
        return self.value
    def _set_stub(self,val):
        print('loop #',self.loops,': ',val)
    def stop(self):
        self.stopped = True
    def setfunc(self,v): #can be overridden by child classes
        #print 'Sweeper setfunc called'
        self._setfunc(v)
    def queryfunc(self): #can be overridden by child classes
        return self._queryfunc()
    def sweep(self,start,stop,step,dt,resume = False,
                loop =  True,
                bidirectional = False,
                maxloops = 0):
        self.stopped = False
        self.maxloops = maxloops
        self.loops = 0
        self.resume = resume
        self.loop = loop
        self.bidirectional = bidirectional
        self.reverse = False
        if start>stop:
            start,stop = stop,start
        if start==stop: #no sweep, just set value
            self.setfunc(start)
            return
        if self.resume:
            self.value = Sweeper.queryfunc(self)
        else:
            if step>0:
                self.value = start
            else:
                self.value = stop
        print('Sweeping from {} to {}, step size is {}'.format(start,stop,step))
        while not self.stopped:
            self.setfunc(self.value)
            if self.verbose:
                #print self.value
                print(self.queryfunc())
            self.value +=step
            if self.value>stop or self.value<start:
            #stop
                if not self.loop:
                    if not self.bidirectional or \
                        self.bidirectional and self.reverse: # two way but backward sweep
                            if self.value>stop:
                                self.value = stop #stop at end
                            elif self.value<start:
                                self.value = start # or at start
                            #self.value = step #stop at end not after end
                            time.sleep(dt)  #make the last step and...
                            self.stopped = True
                            self.setfunc(self.value)
                            if self.verbose: print(self.queryfunc())
                            break #stop sweeping
            #continue
                #bidirectional
                if self.bidirectional:
                    self.reverse = not self.reverse
                    step = step*-1
                    if self.value>=stop: self.value = stop
                    elif self.value<=start: self.value = start
                #unidirectional
                else:
                    if self.value>=stop: self.value = start
                    elif self.value<=start: self.value = stop
                if not self.reverse:
                    self.loops+=1
                if self.maxloops:
                    if self.loops>maxloops: break

            time.sleep(dt)
        print('Sweep has stopped')


if __name__ == '__main__':
    print('test various regimes')
    sw = Sweeper()
    print('up once')
    sw.sweep(0,10,1,.01,loop = False)
    print('down once')
    sw.sweep(3,10,-1,.01,loop = False)
    print('resume up and down once')
    sw.sweep(0,10,1,.01,loop = False,bidirectional = True,resume = True)
    print('loop')
    sw.sweep(0,10,1,.01,maxloops = 3)
