#coding:utf8
import numpy as np
tf = np.genfromtxt('14_time-freq.dat')
ts = np.genfromtxt('14_time-sig.dat')
tmin = max(ts[0,0],tf[0,0])
tmax = min(ts[-1,0],tf[-1,0])
fmin = tf[:,1].min()
fmax = tf[:,1].max()
fstep = abs(np.diff(tf[:,1])).min()
frange = np.arange(fmin,fmax,fstep)
npoints = min(len(ts),len(tf))
t = np.arange(tmin,tmax,(tmax-tmin)/npoints)
tf2 = np.interp(t,tf[:,0],tf[:,1])
sf = np.vstack((tf2,ts[:,1]))
so = sf[:,sf[0].argsort()] #sorted array
al = [] #list of arrays
iprev = 0
for i in xrange(1,len(tf)):
    if tf[i,1]<tf[i-1,1]:
        al.append(tf[iprev:i])
        iprev = i
if i>iprev:
    al.append(tf[iprev:i])
accums = len(al)
k = 0
iprev = 0
al2 = []
for i in xrange(0,len(ts)):
    if ts[i,0]>al[k][:,0].max():
        al2.append(ts[iprev:i])
        k+=1
        if k>=len(al):
            print(ts[i,0],al[k-1][:,0].max())
            break
        iprev=i
if i>iprev:
    al2.append(ts[iprev:i])

x = []
y = []
for i in xrange(0,len(al)):
    tmin = max(al[i][0,0],al2[i][0,0])
    tmax = min(al[i][-1,0],al2[i][-1,0])
    npoints = min(len(al[i]),len(al2[i]))
    t = np.arange(tmin,tmax,(tmax-tmin)/npoints)
    x.append(np.interp(t,al[i][:,0],al[i][:,1]))
    y.append(np.interp(t,al2[i][:,0],al2[i][:,1]))
##for xx,yy in zip(x,y):
##    plot(xx,yy)
i=0
j=0
iprev = 0
jprev = 0
for i in xrange(0,len(tf)):
       if tf[i,0]<ts[0,0]:
           i+=1
       else:
           iprev = i
           break
for j in xrange(0,len(ts)):
       if tf[0,0]>ts[j,0]:
           j+=1
       else:
           jprev = j
           break
rslt = []
while i<len(tf)-1 and j<len(ts)-1:
    tmin = ts[j,0]
    tmax = ts[j+1,0]
    if tf[i+1,0]<tmin:
        i+=1
        while tmin>tf[i,0]:
            i+=1
    wght = (tf[i,0]-tmin)/(tf[i,0]-tf[i-1,0])
    fsum = tf[i,1]*wght
    #fstd =  tf[iprev:i,1].std()
    fnum = wght
    while tf[i,0]<tmax:
        fsum +=tf[i,1]
        fnum+=1
        i+=1
    wght = (tmax-tf[i-1,0])/(tf[i,0]-tf[i-1,0])
    fsum+=tf[i,1]*wght
    fnum+=wght
    faver = fsum/fnum
    #print (j,i,iprev,tf[iprev,0],tf[i,0],ts[j,0])
    iprev=i
    j+=1
    
    rslt.append((faver,ts[j,1]))
urslt = np.array(rslt) #Выглядит хорошо
plot(urslt[:,0],urslt[:,1]) 
rslt = urslt[urslt[:,0].argsort(),:] #Гребенка!
arr = np.zeros(shape = (len(frange),2))

def create_uniform_array(nuarr):
    pass

for i in xrange(0,len(rslt[:,0])):
    idx = frange.searchsorted(rslt[i,0])
    if idx>0 and idx<len(frange):    
        wght_l = (rslt[i,0]-frange[idx-1])/fstep
        wght_r = (frange[idx]-rslt[i,0])/fstep
        print(arr[idx-1,1],wght_l,arr[idx,1],wght_r)        
        wght = arr[idx-1,1]+wght_l
        arr[idx-1,0] =(arr[idx-1,0]*arr[idx-1,1]+rslt[i,1]*wght_l)/wght
        arr[idx-1,1] = wght
        wght = arr[idx,1]+wght_r
        arr[idx,0] =(arr[idx,0]*arr[idx,1]+rslt[i,1]*wght_r)/wght
        arr[idx,1] = wght
    else:
        create_uniform_array(rslt[:,0])
plot(frange,arr[:,0],linewidth = 2)