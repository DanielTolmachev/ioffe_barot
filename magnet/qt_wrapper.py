""" This file allow to quickly wrap any python object into Qt QObject, thus allowing
 to use Qt Signals and Thread

"""
import sys,traceback
from qtpy import QtCore


class QtWrapper(QtCore.QObject):
    """
    
    """
    #public signals
    sigFunctionReturned = QtCore.Signal(str,object)
    sigExceptionRaised = QtCore.Signal(str,object)
    #public methods
    def __init__(self, obj, *,moveToNewThread = True,printExcInfo = True,
                 printReturns = False,verbose = False):
        self._printExcInfo = printExcInfo
        self._printReturns = printReturns
        self.object = None
        self.verbose = verbose
        super(QtWrapper,self).__init__()
        if moveToNewThread:
            self._thrd = QtCore.QThread()
            self._thrd.start()
            self.moveToThread(self._thrd)
        if obj:
            self.setObject(obj)
    def setObject(self, obj):
        """
        This functions set object that will be Qtfied:
        if obj is a typename, we instantiate new object of this type
        directly inside wrapper, that can be useful in some cases if Qtfy object works
        in another thread, in that case wrapped object will be instantiated 
        in that thread 
        :param obj: Object or ClassType or tuple(ClassType,init_args...)        
        """
        if obj:
            # if obj is a typename, we instantiate new object of this type
            # directly inside wrapper, that can be useful if Qtfy object works
            #  in another thread, in that case wrapped object will be instantiated
            # in that thread
            if type(obj)== type: # obj is a type name
                self.object = obj()
            elif isinstance(obj,tuple): # obj is a type name with arguments for initialization
                self.object = obj[0](*obj[1:])
            else:
                self.object = obj
            #find all objects's method                
            attrs = {}
            if hasattr(self.object,"__dir__"):
                for k in self.object.__dir__():
                    if k not in attrs and k[0] != "_":
                        v = getattr(self.object,k,None)
                        if callable(v):                            
                            attrs[k] = v
            for k, v in attrs.items():                
                f = self._Method(self, k)
                if hasattr(self, k):
                    k += "_"
                setattr(self, k, f)
            self._sigCallRequested.connect(self._call__wrapped_func_, QtCore.Qt.QueuedConnection)
            self.sigFunctionReturned.connect(self.dispatchSignals)
    def dispatchSignals(self, func_name, returned_values):
        """
        this method can be reimplemented,
        so you can properly dispatch output of function call:
        for ex. you can format values, so you can show them on label
        :param func_name: name of function, that has returned
        :param returned_values: values returned by function
        """
        if self._printReturns:
            print(func_name,"returned", returned_values)
    #private members
    _sigCallRequested = QtCore.Signal(object, str, tuple, dict)
    _sigObjectSetRequested = QtCore.Signal(object)
    class _Method(QtCore.QObject):
        sigReturned = QtCore.Signal(object)
        def __init__(self,parent,function_name):
            super().__init__()
            self.parent = parent
            self.function_name = function_name
        def __call__(self, *args, **kwargs):
            self.parent._sigCallRequested.emit(self,self.function_name,args,kwargs)
        def connect(self,slot):
            """
            this will connect sigReturned with given slot
            """
            self.sigReturned.connect(slot)
    def _call__wrapped_func_(self, sender,func_name, args, kwargs):
        try:
            if self.verbose:
                print("calling",func_name,*args,kwargs)
            f = getattr(self.object, func_name)
            if self.verbose:
                print(self.object,type(self.object),f,type(f))
            ret = f(*args,**kwargs)
            # if isinstance(ret,tuple):
            #     ret = (func_name,)+ret
            # else:
            #     ret = (func_name,ret)
            sender.sigReturned.emit(ret)
            self.sigFunctionReturned.emit(func_name,ret)
        except:
            exc = sys.exc_info()
            self.sigExceptionRaised.emit(func_name,exc)
            if self._printExcInfo:
                traceback.print_exception(*exc)
                print(func_name,args,kwargs)



if __name__ == "__main__":
    MULTITHREADED = True
    import socket,random
    from qtpy import QtWidgets
    app = QtWidgets.QApplication([])
    port = random.randint(1025,65535)
    soc1 = QtWrapper((socket.socket, socket.AF_INET, socket.SOCK_STREAM),
                    moveToNewThread=MULTITHREADED,printReturns=True)
    soc1.bind(("127.0.0.1", port))
    soc1.listen(5)
    soc1.accept()
    soc2 = QtWrapper((socket.socket, socket.AF_INET, socket.SOCK_STREAM),
                    moveToNewThread=MULTITHREADED, printReturns=True)
    soc2.bind(("127.0.0.1", random.randint(1025,65535)))
    soc2.listen(5)
    soc2.accept()
    #*******************************************************************
    #if you create soc with moveToNewThread=False, program will never end
    #since socket.accept will wait for connection indefinitely
    #*******************************************************************
    soc3 = QtWrapper((socket.socket, socket.AF_INET, socket.SOCK_STREAM))
    soc3.connect(("127.0.0.1", port)) #connect to soc1, soc2 will wait forever
    t = QtCore.QTimer() #quit after 1 second
    t.timeout.connect(app.quit)
    t.start(1000)
    app.exec() #start event loop (so thread can be started also)