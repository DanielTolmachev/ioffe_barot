from __future__ import print_function
try:
    from PyQt4 import uic
except:
    from PyQt5 import uic
import os
#uic.compileUi('py_serial.ui','py_serial_ui.py',False,4,False,False,'_rc')
#uic.compileUiDir('.',False,None)
dl = os.listdir('.')
for fname in dl:
    if fname.endswith('.ui'):
        py_name = fname[:-2]+'py'
        if os.path.exists(py_name):
            if os.path.getmtime(fname)<= os.path.getmtime(py_name):
                continue
        f = open(py_name,'w',encoding="utf8")
        uic.compileUi(fname,f)
        print('%s complied to %s' % (fname,py_name))
        f.close()

