# -*- coding: utf-8 -*-
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"



import sys,traceback
sys.path.append("../modules")

import instrument_base

class ILM(instrument_base.InstrumentBase):
    def __init__(self,*args,**kwargs):
        super(ILM,self).__init__(*args,**kwargs)        
    def getLevel(self,sensor=1):
        rep = self.ask("R{}\r".format(sensor))        
        try:
            if rep and rep.startswith(b"R"):            
                return float(rep[1:].strip())/10
        except:
            traceback.print_exc()
            return rep
    def helium(self):
        return self.getLevel(0)
    def nitrogen(self):
        return self.getLevel(2)
        


if __name__=="__main__":
    DEV_ADDRESS = "com5"    
    ilm = ILM(DEV_ADDRESS)
    ilm.open()
    for i in range(0,15):
#        print("Params {} is {}".format(i,ilm.ask("R{}\r".format(i))))
        print("Params {} is {}".format(i,ilm.getLevel(i)))
    ilm.close()
    # find_gpib(itc)