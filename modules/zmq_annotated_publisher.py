from __future__ import print_function
import zmq_publisher
import sys,json,os
from collections import OrderedDict

__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev"

class Zmq_Pub_Annotated(zmq_publisher.Zmq_Publisher):
    def __init__(self,context=None,address=None,name = "", description = "", fieldnames = [], fieldunits = [], types = [], **kwargs):
        self.fieldnames = self._check_names_list(fieldnames)
        self.fieldunits = self._check_names_list(fieldunits)
        self.types = self._check_names_list(types)
        self.name = name
        path = sys.argv[0]
        if not self.name:
            name = os.path.basename(path)
            self.name, _ = os.path.splitext(name)
        self.dict = OrderedDict()
        self.dict["name"] = name
        self.dict["Messaging library"] = "ZMQ"
        self.dict["Messaging mode"] = "ZMQ.PUB"
        self.dict["address"] = address
        self.dict["app_path"] = path
        if description:
            self.dict["description"] = description
        if self.fieldnames:
            self.dict["channels_names"] = self.fieldnames
        if self.fieldunits:
            self.dict["channels_units"] = self.fieldunits
        if self.types:
            self.dict["channels_types"] = self.types
        for k,v in kwargs.items():
            if k not in self.dict:
                self.dict[k] = v
        super(Zmq_Pub_Annotated,self).__init__(context,address)
    def _check_names_list(self,arg):
        if isinstance(arg,tuple):
            arg = list(arg)
        elif not isinstance(arg,list):
            arg = [arg]
        for i,el in enumerate(arg):
            arg[i] = str(el)
        return arg
    def bind(self):
        super(Zmq_Pub_Annotated,self).bind()
        self.annotate()
    def annotate(self):
        if self.bound:
            home = os.path.expanduser("~")
            folder = home+os.sep+".lab_python_apps"
            if not os.path.exists(folder):
                os.mkdir(folder)
                print("created folder", folder)
            port = self.address[self.address.rfind(":")+1:]
            file = folder+os.sep+"zmq_pub_port"+str(port)+"_" +self.name+".json"
            with open(file,"w+") as f:
                json.dump(self.dict,f,indent=4)


