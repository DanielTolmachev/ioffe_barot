import logging
from magnet_IPS_class import *
from qtpy import QtCore
import qt_wrapper
from math import isclose,nan
#class magnet_control(QtCore.QEventLoop):
class magnet_control(qt_wrapper.QtWrapper):
    dt = 100
    field = 0
    running = False
    sig_field = QtCore.Signal(float)
    sig_field_PSU = QtCore.Signal(object)
    sig_started = QtCore.Signal()
    sig_stopped = QtCore.Signal()
    sig_connected = QtCore.Signal(object)
    sig_heater_status = QtCore.Signal(float)
    sigStatusChanged = QtCore.Signal(dict)
    next = False
    def __init__(self,mg,options_dict = {},verbose = False,
                 timeout_slow = 1,timeout_fast = 0.1):
        super(magnet_control,self).__init__(mg,verbose = verbose)
        self.setRemote = self._printargs
        self.status_string = ""
        #self.mg = magnet_IPS()
        self.mg = mg
#        self.field_is_changing = 0        
        self.field = self.field_PSU = self.target = nan
        self.sweep_on = False
        self.opt = options_dict
        self.timer_ststus = None
        self.timer_field = None
        self.heater = None
        self.TIMEOUT_SLOW = timeout_slow*1000 # timeout in ms
        self.TIMEOUT_FAST = timeout_fast*1000
        self.timeout = self.timeout_field = self.TIMEOUT_SLOW
#        self.sig_started.emit()
#         self.queryField.connect(self.onFieldUpdate)
        # self.queryFieldPersistent.connect(self.onFieldPersistentUpdate)
        # self.queryStatus.connect(self.onStatusUpdate)
        self.sig_started.connect(self._setRunning)
#        self.sig_stopped.connect(self._setStopped)
        # self.setHeater.connect(self.onHeaterChange)
        self.open.connect(self._onOpen)        # self.sig_stopped.connect(self.cont)
#        self.tasks = [self.queryStatus,self.queryField,self.queryFieldPersistent]
    def _queryDeviceStatus(self):
        """
        do not call from GUI thread
        """
        if not self.timer_ststus.isActive():
            self.timer_ststus.start(self.timeout)
        status = self.mg.queryStatus()        
        if status:            
            if status["string"]!=self.status_string:                
                self.sigStatusChanged.emit(status)
                self.status_string = status["string"]
                if "remote" in status:
                    self.remote = status["remote"]
            self.onStatusUpdate(status)
            print(status["string"],"heater",self.heater)            
            if self.running:
                if status["A"] in [0,4]:  #we stopped using Hold or Clamped mode
                    self._setStopped()
                elif status["A"] == 2 and isclose(self.field_PSU,0): #we have reached zero
                    self._setStopped()
                elif status["A"] == 1:
                    self.target = self.mg.queryFieldTarget()
#                    print(self.running,self.field_PSU,self.target)
                    if isclose(self.field_PSU,self.target): #we have reached zero
                        self._setStopped()
            else:
                if status["A"] == 2 and not isclose(self.field_PSU,0): #we are moving to zero
                    self.sig_started.emit()
                elif status["A"] == 1: #we are moving to target field
                    self.target = self.mg.queryFieldTarget()                                        
#                    print(self.running,self.field_PSU,self.target)
                    if not isclose(self.field_PSU,self.target): #but have not reached it yet
                        self.sig_started.emit()
            if self.heater in [0,2]:
                persfield = self.mg.queryFieldPersistent()
#                print("persistent field",persfield)
                if persfield is not nan:
                    self.field = persfield
                    self.sig_field.emit(persfield)

    def _queryFieldPSU(self):
        """
        do not call from GUI thread
        """
        self.timer_field.start(self.timeout_field)
        fieldPSU = self.mg.queryField()
        if fieldPSU is not None:
            if self.heater in [0,2]:
                self.sig_field_PSU.emit(self.field_PSU)
            else:
                self.sig_field.emit(self.field_PSU)
                self.field = self.field_PSU
                self.sig_field_PSU.emit(None)
             #if value is not changing or have reached target
            if self.running and (
                    self.field_PSU==fieldPSU or isclose(fieldPSU,self.target)):
                print("sweep is prorbaly finished")
                self.timer_ststus.stop()
                self.timer_ststus.start(0) #we need to check status
            self.field_PSU = fieldPSU
    def _printargs(*args,**kwargs):
        print(args,kwargs)
    def _setRunning(self):
        print("set running")
        self.running = True
        self.timeout_field = self.TIMEOUT_FAST
        self.timer_field.stop()
        self.timer_field.start(0) #update FieldPSU immidiately
        self.timer_ststus.stop()
    def _setStopped(self):
        if self.sweep_on and self.loop:
            self.sweepBack()
        else:
            self.sig_stopped.emit()
            print("set running off (slow mode)")
            self.running = False
            self.timeout_field = self.TIMEOUT_SLOW
            if not self.timer_ststus.isActive():
                self.timer_ststus.start(self.timeout)
    def _init_timers(self):
        if not self.timer_ststus:
            self.timer_ststus = QtCore.QTimer()
            self.timer_ststus.setSingleShot(True)
            self.timer_ststus.timeout.connect(self._queryDeviceStatus)
        self.timer_ststus.start(self.timeout)
        if not self.timer_field:
            self.timer_field = QtCore.QTimer()
            self.timer_field.setSingleShot(True)
            self.timer_field.timeout.connect(self._queryFieldPSU)
        self.timer_field.start(self.timeout_field)
    def onStatusUpdate(self,status):
#        print(status_string)
        if "heater" in status:
            htr = status["heater"]                       
            if self.heater in [-1,-2]:
                if self.heater==-1:
                    # we want to switch on heater
                    print("preparing for switching on heater",htr)
                    if htr == 2: # heater is off, and field is matching
                        print("sending command heater ON")
                        self.setHeater_(True)
                    elif htr==1:
                        self.heater = 1
                        print("heater is on")
                    elif htr == 0 and isclose(self.field,0): # heater is off, and field is zero
                        print("sending command heater ON")
                        self.setHeater_(True)
                    
                    #or we wait till current matches magnetic field                                        
                elif self.heater==-2:
                    #we want to switch heater off
                    if htr == 2:
                        #we need to set current to zero
                        print("heater is off, setting field to zero")
                        self.gotoZero()
                        self.heater = htr
                    elif htr==0:
                        print("heater is off, field at zero")
                        #current is alredy at zero
                        self.heater = htr                
            else:
                self.heater = htr
            self.sig_heater_status.emit(self.heater)
              
    def sweepBack(self):
        print("turning into opposite direction")
        if self.target==self.sweep_stop:
            target = self.sweep_start
        else:
            target = self.sweep_stop
        self.sweep_single(target,self.sweep_speed,self.sweep_dt)
    def onFieldUpdate(self,val):
        if val is not None:
            if val!=self.field_PSU:
#                self.field_is_changing |= 0b01
                self.field_PSU = val
#            else:
#                self.field_is_changing &= 0b10
            if self.heater in [0,2]:                 
                self.sig_field_PSU.emit(self.field_PSU)          
            else:                
                if val==self.field_PSU:
                    self.field_is_changing = False
                self.sig_field.emit(self.field_PSU)
                self.sig_field_PSU.emit(None)
    def onFieldPersistentUpdate(self,val):
#        print("persistent field",val)        
        if val is not None:
            if val!=self.field:
                self.field_is_changing |= 0b10
                self.field = val
            else:
                self.field_is_changing &= 0b01
            if self.heater in [0,2]:
                self.sig_field.emit(val)
#    def onHeaterChange(self,status_string):
#        print("heater status_string",status_string)
#     def dispatch(self):
#         for func in self.tasks:
#             func()
# #        print("field changing: {}".format(self.field_is_changing))
#         if self.field_is_changing:
#             self.timeout = self.TIMEOUT_FAST*1000
#         else:
#             if self.sweep_on:
#                 if self.loop:
#                     self.sweepBack()
#                 else:
#                     self.sweep_on = False
#                     self.sig_stopped.emit()
#                     self.timeout = self.TIMEOUT_SLOW*1000
#             else:
#                 self.timeout = self.TIMEOUT_SLOW*1000
#         self.timer_ststus.start(self.timeout)
#    def update(self):
#        fld = self.mg.queryField()
#        if fld!=None:
#            if fld > self.target and self.field is not None and fld > self.field:
#                logging.debug('Field value omitted {} {}'.format(fld,self.field))
#            else:
#                self.sig_field.emit(fld)
#                self.field = fld
#        #if self.running:
#        if fld == self.target:
#            logging.debug('field reached {}'.format(self.target))
#            self.running = False
#            self.mg.stop()
#            self.timer_ststus.stop()
#            self.cont()
            # self.sig_stopped.emit()
    def stop(self):
        self.mg.stop()
        self.next = False
        self.sweep_on = False
#        self.timer_ststus.stop()
#        fld = self.mg.queryField()
#        if fld is not None:
#            self.sig_field.emit(fld)
        self.sig_stopped.emit()
    def sweep_single(self,target,rate,dt):
        self.dt = dt
        if target > MAXFIELD:
            target = MAXFIELD
        for i in reversed(range(0, len(FIELDS))):
            if target > FIELDS[i]:
                if rate > RATES[i]:
                    rate = RATES[i]
                    logging.info('rate reduced to {} T/min'.format(rate*60))
                break

        self.setFieldRate(rate)
        self.setFieldTarget(target)
        # target = self.mg.queryFieldTarget()
        # rate = self.mg.queryFieldRate()
        # if not rate or target is None:
        #     return
        logging.info('Changing field from {} to {} at {} T/min'.format(self.field,target,rate*60))
        self.running = True
        self.target = target
        self.go()
#        self.field_is_changing = True 
#        self.timer_ststus.start(self.dt)
        self.sig_started.emit()
    def findMaxRate(self,field,dir):
        for i in reversed(range(0, len(FIELDS))):
            if dir>0: #up
                if field >= FIELDS[i]:
                    rate = RATES[i]
                    #print 'rate reduced to {} T/min'.format(rate*60)
                    if i<len(FIELDS)-1:
                        return rate, FIELDS[i+1] #returning next field
                    else:
                        return rate, MAXFIELD #returning next field
            if dir<0: #up
                if field > FIELDS[i]:
                    rate = RATES[i]
                    #print 'rate reduced to {} T/min'.format(rate*60)
                    return rate, FIELDS[i]
        if dir>0:
            return MAXRATE, FIELDS[0]
        else:
            return MAXRATE, 0
    def setField(self,target):
#        self.timeout = self.TIMEOUT_FAST        
        logging.info('Changing field to {}'.format(target))
        self.getField()
        if self.field is not None:
            # if fld:
            #     self.field = fld
            if target>self.field: #we are going up
                dir = +1
            else:
                dir = -1
            curRate, fld = self.findMaxRate(self.field,dir)
            logging.info ('Will stop at {}, rate is {}'.format(fld,curRate*60))
            trgRate, _ = self.findMaxRate(target,dir)
            if curRate == trgRate:
                self.sweep_single(target,curRate,self.dt)
                self.next = False
            else:
                self.sweep_single(fld,curRate,self.dt)
                self.next = target
        else:
            logging.info("current field unknown, cannot proceed")
    def getField(self):
        fld = self.mg.queryField()        
        if fld is not None:
            self.field = fld
            self.sig_field.emit(fld)
            return fld
        else:
            print("connected",self.mg.interface.isOpen())
    def cont(self):
            if type(self.next)==float:
                self.setField(self.next)
                return
            elif self.sweep_on:
                if self.bidirectianal and self.loops%1: #sweep backwards
                    self.sweep(self.sweep_start, self.sweep_stop, -self.sweep_speed, self.sweep_dt,
                               loop = self.loop, bidirectional=self.bidirectianal, maxloops=self.loops)
                    return
                elif self.loop and self.loops: #sweep in reversed direction
                    self.sweep(self.sweep_start,self.sweep_stop,-self.sweep_speed,self.sweep_dt,
                               loop=self.loop, bidirectional=self.bidirectianal, maxloops=self.loops)
                    return
            self.sig_stopped.emit()
    def sweep(self,start,stop,speed,dt,resume = False,
                loop =  True,
                bidirectional = False,
                maxloops = 0):
        logging.info("sweep {:g} - {:g}T, {:g}T/s, {:g}s, loop:{}, bidirectianal:{}, maxloops:{}".format(start,stop,speed,dt,loop,bidirectional,maxloops))
        self.field = self.mg.queryField()
        if speed <0:
            start,stop = stop,start

        self.sweep_start = start
        self.sweep_stop = stop
        self.sweep_speed = speed
        self.sweep_dt = dt
        self.loop = loop
        self.bidirectianal = bidirectional
        self.loops = maxloops-0.5
        self.sweep_on = True
        self.field_is_changing = 3
        self.sweep_single(stop,abs(speed),dt)
#    def open(self):
#        try:
#            logging.info("opening magnet")
#            self.mg.open()
#            logging.info("magnet id returned: {}".format(self.mg.getID()))
#            self._onOpen()
#            self.mg.setRemote()
#            self.mg.setLF()
#            self.heater = self.mg.getHeater()
#            self.sig_heater_status.emit(self.heater)
#            logging.debug("setting LF, magnet id returned: {}".format(self.mg.getID()))
#        except Exception as err:
#            if err.args and err.args[0].startswith("could not open port"):
#                errmsg = ""
#            else:
#                errmsg = str(err)
#            logging.exception('cannot connect to "{}" {}'.format(self.mg.dev_address,errmsg))
#            self._onOpen(errmsg)
    def close(self):
        self.mg.close()
        self._onOpen()
    def _onOpen(self, error_msg = None):
        if error_msg is None:
            if self.mg.interface.isOpen():
                msg = "connected to {}".format(self.mg.dev_address)
                self.sig_connected.emit(msg)
                logging.info(msg)                
                self.setLF()
                self.tasks = [self.queryStatus,self.queryField,self.queryFieldPersistent]
            else:
                msg = 'disconnected, "{}"'.format(self.mg.dev_address)
                self.sig_connected.emit(msg)
                logging.info(msg)
        else:
            self.sig_connected.emit('cannot connect to "{}" {}'.format(self.mg.dev_address, error_msg))
            self.tasks = [self.open]
        self._init_timers()
    def setAddress(self,address):
        self.mg.setAddress(address)
        self.opt["device.address"] = address
        self._onOpen()
    def setHeater(self,status):        
#        self.setHeater_(status_string)
        if status: #we want to switch heater on
            self.heater = -1 #we want to switch on heater
            self.setField(self.field)            
        else:
            self.heater = -2 #we want to switch off heater, and reduce current to 0
            self.setHeater_(status)
            


if __name__ == '__main__':
    from qtpy import QtWidgets
    app = QtWidgets.QApplication([])
    mg = magnet_IPS ('serial','COM8')
    mc = magnet_control(mg)
    print (mg.getID())
    # fld = mg.queryField()
    # print fld
    # if fld>0.5:
    #     mg.setField(0.)
    # else:
    #     mg.setField(1.)
    print (mg.queryField())
    mc.sweep_single(1.,.4)

    mc.exec_()
    mg.setField(0.)