from magnet_IPS_class import *
from qtpy import QtCore
class magnet_control(QtCore.QEventLoop):
    dt = 100
    field = 0
    running = False
    sig_field = QtCore.Signal(float)
    sig_started = QtCore.Signal()
    sig_stopped = QtCore.Signal()
    sig_connected = QtCore.Signal(object)
    next = False
    def __init__(self,mg,options_dict = {}):
        super(magnet_control,self).__init__()
        #self.mg = magnet_IPS()
        self.mg = mg
        self.opt = options_dict
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        self.sig_started.emit()
        self.sig_stopped.connect(self.cont)
    def update(self):
        fld = self.mg.queryField()
        if fld!=None:
            if fld > self.target and fld > self.field:
                print ('Field value omitted {} {}'.format(fld,self.field))
            else:
                self.sig_field.emit(fld)
                self.field = fld
        #if self.running:
        if fld == self.target:
            print ('field reached {}'.format(self.target))
            self.running = False
            self.mg.stop()
            self.timer.stop()
            self.sig_stopped.emit()
    def stop(self):
        self.mg.stop()
        self.next = False
        fld = self.mg.queryField()
        self.sig_field.emit(fld)
        self.sig_stopped.emit()
    def sweep_single(self,target,rate,dt):
        self.dt = dt
        if target > MAXFIELD:
            target = MAXFIELD
        for i in reversed(range(0, len(FIELDS))):
            if target > FIELDS[i]:
                if rate > RATES[i]:
                    rate = RATES[i]
                    print ('rate reduced to {} T/min'.format(rate*60))
                break

        self.mg.setFieldRate(rate)
        self.mg.setFieldTarget(target)
        target = self.mg.queryFieldTarget()
        rate = self.mg.queryFieldRate()
        print ('Changing field from {} to {} at {} T/min'.format(self.field,target,rate*60))
        self.running = True
        self.target = target
        self.mg.go()
        self.timer.start(self.dt)
        self.sig_started.emit()
    def findMaxRate(self,field,dir):
        for i in reversed(range(0, len(FIELDS))):
            if dir>0: #up
                if field >= FIELDS[i]:
                    rate = RATES[i]
                    #print 'rate reduced to {} T/min'.format(rate*60)
                    if i<len(FIELDS)-1:
                        return rate, FIELDS[i+1] #returning next field
                    else:
                        return rate, MAXFIELD #returning next field
            if dir<0: #up
                if field > FIELDS[i]:
                    rate = RATES[i]
                    #print 'rate reduced to {} T/min'.format(rate*60)
                    return rate, FIELDS[i]
        if dir>0:
            return MAXRATE, FIELDS[0]
        else:
            return MAXRATE, 0
    def setField(self,target):
        print ('Changing field to {}'.format(target))
        self.field  = self.mg.queryField()
        # if fld:
        #     self.field = fld
        if target>self.field: #we are going up
            dir = +1
        else:
            dir = -1
        curRate, fld = self.findMaxRate(self.field,dir)
        print ('Will stop at {}, rate is {}'.format(fld,curRate*60))
        trgRate, _ = self.findMaxRate(target,dir)
        if curRate == trgRate:
            self.sweep_single(target,curRate,self.dt)
            self.next = False
        else:
            self.sweep_single(fld,curRate,self.dt)
            self.next = target
    def getField(self):
        fld = self.mg.queryField()
        if fld != None:
            self.sig_field.emit(fld)
    def cont(self):
            if type(self.next)==float:
                self.setField(self.next)
    def sweep(self,start,stop,speed,dt,resume = False,
                loop =  True,
                bidirectional = False,
                maxloops = 0):
        #print start,stop,speed,dt
        self.field = self.mg.queryField()
        if speed <0:
            start,stop = stop,start
        self.sweep_single(stop,abs(speed),dt)
    def open(self):
        self.mg.open()
        self.updateConnectionStatus()
    def close(self):
        self.mg.close()
        self.updateConnectionStatus()
    def updateConnectionStatus(self):
        if self.mg.interface.isOpen():
            self.sig_connected.emit("connected to {}".format(self.mg.dev_address))
        else:
            self.sig_connected.emit('disconnected, "{}"'.format(self.mg.dev_address))
    def setAddress(self,address):
        self.mg.setAddress(address)
        self.opt["device.address"] = address
        self.updateConnectionStatus()


if __name__ == '__main__':
    from qtpy import QtWidgets
    app = QtWidgets.QApplication([])
    mg = magnet_IPS ('serial','COM8')
    mc = magnet_control(mg)
    print (mg.getID())
    # fld = mg.queryField()
    # print fld
    # if fld>0.5:
    #     mg.setField(0.)
    # else:
    #     mg.setField(1.)
    print (mg.queryField())
    mc.sweep_single(1.,.4)

    mc.exec_()
    mg.setField(0.)