#-------------------------------------------------------------------------------
# Name:     Magnet controller
# Purpose:  control magnet field
#           using Qt user interface and
#           publish data using ZMQ messaging library
#
# Author:      Daniel Tolmachev (Daniel.Tolamchev@mail.ioffe.ru)
#
# Created:     16.03.2015
#
#-------------------------------------------------------------------------------
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@gmail.com/Daniel.Tolmachev@mail.ioffe.ru)"

DEV_ADDRESS = 'COM1'
DEV_INTERFACE = 'serial' # lan or serial or dummy
PUBLISH_ADDRESS = 'tcp://127.0.0.1:5006'

START = 0 #mA
STOP = 300 #mA
STEP = 0.2 #mA
DT= 0.3 #s


import sys,time
sys.path.append( '../modules')
import enable_exception_logging
from qtpy import QtCore,QtWidgets
import zmq
from magnet_IPS_class import magnet_IPS
from magnet_control import magnet_control
from zmq_publisher import Zmq_Publisher
import sweep_widget,quickQtApp,device_address_widget,QMenuFromDict,zmq_annotated_publisher

class SignalProxy(QtCore.QObject):
    sig_float = QtCore.Signal(object)
    sig_tuple = QtCore.Signal(tuple)
    sig_string = QtCore.Signal(str)
    sig = QtCore.Signal(object)
    def rcv(self,t):
        #print type(t),': ',t
        if type(t)==tuple:
            self.sig.emit(t)
            if len(t)>1:
                if type(t[1])==float:
                    self.sig_float.emit(t[1])
        elif type(t)==str:
            self.sig.emit(t)
        elif type(t)==float:
            self.sig.emit((time.time(),t))
            #print t
    def setqueryFreq(self,v):
        f = mg.setqueryFreq(v)
        self.sig_float.emit(f)
signalproxy = SignalProxy()
context = zmq.Context()
app,opt,win,cons = quickQtApp.mkAppWin('Magnet power supply')
pub = zmq_annotated_publisher.Zmq_Pub_Annotated(address=PUBLISH_ADDRESS,
                                                fieldnames=["time","Magnetic field"],
                                                fieldunits=["s","T"],
                                                name="Magnet",
                                                description="Magnet controller by Oxford Instruments")
mg = magnet_IPS(opt.get("device.interface",DEV_INTERFACE), opt.get("device.address", DEV_ADDRESS))
conn_status = QtWidgets.QLabel('disconnected, "{}"'.format(mg.dev_address))
win.statusBar().addWidget(conn_status)
mc = magnet_control(mg,options_dict = opt,verbose = False)
gl = QtWidgets.QGridLayout()

class MagnetSweepWidget(sweep_widget.SweepWidget):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.lbl_field_psu = QtWidgets.QLabel(self)
        self.lbl_field_psu.setGeometry(QtCore.QRect(20, 60, 113, 42))
    def showFieldPSU(self,val):        
        if val is not None:
#            self.lbl_field_psu.setEnabled(True)
            self.lbl_field_psu.setText("PSU field\n{} T".format(val))
        else:
            self.lbl_field_psu.setText("")
#            self.lbl_field_psu.setEnabled(False)
    def onStatusChange(self):
        pass
sw = MagnetSweepWidget(units = 'T',
                      sweepValueDesc = 'SweepField',
                      units_multiplier = 1 ,
                      step_as_speed = True,
                      units_speed = 'T/m',
                      units_speed_multiplier = 1./60,
                      auto_switch_direction = True)
sw.le_dt.setEnabled(False)
sw.tb_dt_up.hide()
sw.tb_dt_dn.hide()

w_addr = device_address_widget.DeviceAddressWidget(mg.dev_address, interfaces=["lan","serial"], default_ip_port=DEV_ADDRESS)
w_addr.sigConnectionRequested.connect(mg.open)
w_addr.sigNewAddressSet.connect(mc.setAddress)

QMenuFromDict.AddMenu(win.menuBar(),[
                                    ("device",[
                                                ("connect",mc.open),
                                                ("disconnect",mc.close),
                                                ("change address",w_addr.show),
                                                ("set Remote and Unlocked",lambda *x:mc.setRemote)
                                                ]),
                                    ])
class MagnetControls(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.gl = QtWidgets.QGridLayout()
        self.setLayout(self.gl)
        self.btn_zero = QtWidgets.QToolButton()
        self.btn_zero.setText("Zero field")
        self.gl.addWidget(self.btn_zero,0,0)
        self.btn_heater = QtWidgets.QToolButton()
        self.btn_heater.setCheckable(True)
        self.btn_heater.setText("Heater")
        self.gl.addWidget(self.btn_heater,0,1)
    def showHeaterStatus(self,value):
        self.btn_heater.blockSignals(True)
        if value==1:
            self.btn_heater.setEnabled(True)
            self.btn_heater.setChecked(True)
            self.btn_heater.setText("heater ON")
        elif value==-1:
            self.btn_heater.setEnabled(True)
            self.btn_heater.setChecked(True)
            self.btn_heater.setText("switching ON")
        elif value in [0,2,-2]:
            self.btn_heater.setEnabled(True)
            self.btn_heater.setChecked(False)
            self.btn_heater.setText("heater OFF")
        else:
            self.btn_heater.setEnabled(False)
        self.btn_heater.blockSignals(False)
    def onStatusChange(self):
        pass
    
mcw = MagnetControls()
    
_wdgt = QtWidgets.QWidget()
_wdgt.setLayout(QtWidgets.QGridLayout())
_wdgt.layout().addWidget(sw)
_wdgt.layout().addWidget(mcw)
win.setCentralWidget(_wdgt)
# btn_zero = QtGui.QPushButton('ToZero')
# gl.addWidget(btn_zero,1,0)
# gl.addWidget(sw,0,1)
# sw_field = SweeperWMsg(  messager = signalproxy.rcv,
#                         setfunc = mg.setField,
#                         queryfunc = mg.queryField,
#                         verbose = False)
#sw_freq.set_units(1e6,'MHz')
#sw.setunits('V',1)



sw.connect_options_dictionary(opt)
sw.sig_params_changed.connect(opt.savetodisk) #save options immidiately
#cmd_freq = CmdProxy(sw,sw_field,pub)
#sw.sig_started.connect(cmd_freq.cmd_parse)

mcw.btn_heater.toggled.connect(mc.setHeater)
mcw.btn_zero.pressed.connect(mc.gotoZero)
mc.sig_heater_status.connect(mcw.showHeaterStatus)
mc.sigStatusChanged.connect(mcw.onStatusChange)
mc.sigStatusChanged.connect(sw.onStatusChange)
sw.sig_start_sweeper.connect(mc.sweep)
sw.sig_stop_sweeper.connect(mc.stop)
#sw.sig_start_sweeper.connect(sw_field.sweep)
#sw.sig_stop_sweeper.connect(sw_field.stop)

mc.sig_started.connect(sw.setGuiStarted)
mc.sig_stopped.connect(sw.setGuiStopped)

sw.sig_started.connect(signalproxy.rcv)
#sw.sig_started.connect(mg.setlocal) #for convenience
sw.sig_value_set.connect(mc.setField) #set current from GUI widget
signalproxy.sig.connect(pub.send_pyobj) #send tuple away using zmq
signalproxy.sig_float.connect(sw.set_central_label) #modify widget central value
mc.sig_field.connect(sw.set_central_label)
mc.sig_field.connect(signalproxy.rcv)
mc.sig_field_PSU.connect(sw.showFieldPSU)
#sw.set_central_label(mg.queryField())

mc.sig_connected.connect(conn_status.setText)

#First Simple message box variant
##text ='''Sweeping current from {} to {}
##Time per sweep is {}'''.format(START,STOP,(STOP-STEP)/STEP*DT)
##mb = QtGui.QMessageBox()
##mb.addButton('Stop',QtGui.QMessageBox.YesRole)
mc.open()
mc.queryField()
win.show()
app.exec_()
opt.savetodisk()
mg.close()
app.quit()



