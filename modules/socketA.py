#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      SFB
#
# Created:     04.12.2015
# Copyright:   (c) SFB 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import socket,traceback,threading,time,six

class SocketA(object):
    connected = False
    keep_connection = False
    autoconnect = False
    address = None
    sock = None
    timeout = 1
    recon_interval = 1
    recon_active = False
    def __init__(self, *address, **kwargs):
        """
        address = ("192.168.0.4",1234)
        kwargs:
            timeout = 1
            autoconnect = 0
        """
        self._parseargs(*address, **kwargs)
        self.recon_timer = threading.Timer(self.recon_interval,self.reconnect)
    def _parseargs(self,*address, **kwargs):
        if "timeout" in kwargs:
            self.timeout = kwargs["timeout"]
        if "autoconnect" in kwargs:
            self.autoconnect = kwargs["autoconnect"]
        if address and type(address[0])==tuple:
            self.address = address[0]
            print("address = ",self.address)
    def open(self, *address, **kwargs):
        """
        address = ("192.168.0.4",1234)
        kwargs:
            timeout = 1
        """
        self._parseargs(*address, **kwargs)

        # print("SocketA.open")
        #print(caller_name())

        try:
            print("connecting...",end="")
            self.sock = socket.create_connection(self.address,timeout = self.timeout)
            self.connected = True
            if self.autoconnect:
                self.keep_connection = True
            msg = "connected to: {}".format(self.address)
            print(msg)
            return msg
        except socket.timeout:
            msg = "timed out ({})".format(self.timeout)
            return msg
        except:
            traceback.print_exc()
    def close(self):
        print("SocketA: closing socket")
        if self.sock:
            self.sock.close()
        self.connected = False
        self.keep_connection = False
        self.recon_timer.cancel()
    def send(self,*args,**kwargs):
        rep = self.sock.send(*args,**kwargs)
        return rep
    def recv(self,*args,**kwargs):
        rep = self.sock.recv(*args,**kwargs)
        return rep
    def read(self):
        try:
            if self.connected:
                return self.recv(1024)
            else:
                if self.keep_connection and not self.recon_timer.is_alive():
                    self.recon_timer.start()
                else:
                    return b""
        except:
            self.onException()
    def write(self,data):
        try:
            if self.connected:
                if six.PY2:
                    return self.send(data)
                else:
                    return self.send(bytes(data,"utf8"))
            else:
                if self.keep_connection and not self.recon_timer.is_alive():
                    self.recon_timer.start()
        except:
            self.onException()
    def onException(self):
        traceback.print_exc()
    def reconnect_start(self):
        self.recon_timer.start()
    def reconnect(self):
        while not self.connected and self.keep_connection:
            print("trying to reconnect ", end=' ')
            #print self.connected,self.keep_connection
            res = self.open()
            print(res)
            time.sleep(self.recon_interval)
    # def reconnect(self):
    #     if not self.connected and self.keep_connection:
    #         print "trying to reconnect ",
    #         #print self.connected,self.keep_connection
    #         res = self.open()
    #         print res
    #         if not self.connected and self.keep_connection:
    #             print self.recon_timer.is_alive()
    #             self.recon_timer.start()
    #     else:
    #         self.recon_timer.cancel()


if __name__ == '__main__':
    soc = SocketA(("129.217.155.192",1234),timeout = 1)
    soc.open()
