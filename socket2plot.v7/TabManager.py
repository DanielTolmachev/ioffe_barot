from __future__ import print_function
__author__ = 'SFB'

from qtpy import QtCore
import plot_manager,curve_manager

ttTabLive = """This tab shows live view of signal received by program
They are displayed in a manner similar to oscilloscope without saving"""

ttTabYT = "this tab dispalyes recorded signals as a fuction of time\n(this is a way a data are received)"

NEED_SHOW_X_PLOT = False

class TabManager(QtCore.QObject):
    xyPlotManagersDict = dict();xyCurveManagersDict = dict()
    NAME_TAB_BY_UNITS = 0
    NAME_TAB_BY_XTITLE = 1
    NAME_TAB_BY_DEVICE_NAME = 2
    tabnaming = NAME_TAB_BY_DEVICE_NAME
    sigMouseMoved = QtCore.Signal(float,float)
    def __init__(self,wnd,autoCreatePlots = False):
        super(TabManager,self).__init__()
        self.wnd = wnd
        self.tw = wnd.tabWidget
        self.xytab_src_numbers  = []
        self.srcl = wnd.srcl
        self.autoCreatePlots = autoCreatePlots
        self.plotManagerLivemode = plot_manager.PlotManager()
        self.plotManagerLivemode.sigMouseMoved.connect(self.sigMouseMoved)
        self.tw.addTab(self.plotManagerLivemode, "Live")
        self.tw.setTabToolTip(0,ttTabLive)
        self.curveManagerLivemode = curve_manager.CurveManager(wnd.srcl)
        self.curveManagerLivemode.plotManager = self.plotManagerLivemode
        self.curveManagerLivemode.autoCreatePlots = self.autoCreatePlots
        #self.curveManagerLivemode.setupplot_live()




        self.plotManagerTimemode = plot_manager.PlotManager()
        self.plotManagerTimemode.sigMouseMoved.connect(self.sigMouseMoved)
        self.tw.addTab(self.plotManagerTimemode, "y(t)")
        self.tw.setTabToolTip(1,ttTabYT)
        self.curveManagerTimemode = curve_manager.CurveManager(wnd.srcl)
        self.curveManagerTimemode.plotManager = self.plotManagerTimemode
    def new(self):
        """
        notify all CurveManagers that we have started new recording
        """
        self.curveManagerLivemode.new()
        self.curveManagerTimemode.new()
        for curve_manager in self.xyCurveManagersDict.values():
            curve_manager.new()

#        self.tlive.addWidget(self.plotManagerLivemode)
 #       self.ttime.addWidget(self.plotManagerTimemode)
    def createXYtab(self,srcN):
        src = self.srcl[srcN]
        if not srcN in self.xyPlotManagersDict:
            if hasattr(src,'xlabel'):
                            self.xlabel = src.xlabel
                            #self.pi.setLabel('bottom',self.xlabel)
                            if hasattr(src,'xunits'):
                                self.xunits = src.xunits
            if self.tabnaming == self.NAME_TAB_BY_UNITS and hasattr(src,'xunits'):
                name = src.xunits
            elif self.tabnaming == self.NAME_TAB_BY_XTITLE and hasattr(src,'xlabel'):
                name = src.xlabel
            else:
                name = src.name
            tooltip = src.name
            if hasattr(src,'description'):
                tooltip += "\n{}".format(src.description)
            if hasattr(src,'xlabel'):
                tooltip += "\n{}".format(src.xlabel)
            if hasattr(src,'xunits'):
                tooltip += " ({})".format(src.xunits)
            if hasattr(src,'comment'):
                tooltip += "\n{}".format(src.comment)
            print("TabManager: creating tab \"{}\"".format(name))
            self.xyPlotManagersDict[srcN]=plot_manager.PlotManager()
            self.xyPlotManagersDict[srcN].sigMouseMoved.connect(self.sigMouseMoved)
            self.xyCurveManagersDict[srcN]=curve_manager.CurveManager(self.srcl)
            self.xyCurveManagersDict[srcN].plotManager = self.xyPlotManagersDict[srcN]
            print(self.xyCurveManagersDict[srcN].plotManager)
            if not NEED_SHOW_X_PLOT:
                self.xyCurveManagersDict[srcN].setX(srcN)
            self.xytab_src_numbers.append(srcN)
            self.tw.addTab(self.xyPlotManagersDict[srcN], name)
            self.tw.setTabToolTip(self.tw.count()-1,tooltip)
    def tab_index2src_number(self,idx):
        return self.xytab_src_numbers[idx-2]
    def inputModified(self):
        print("TabManager: input sources was modified",self.wnd.xyplot)
        if self.wnd.xyplot:
            srcN, chan = self.wnd.xyplot
            if srcN not in self.xyPlotManagersDict:
                self.createXYtab(srcN)

    # def setCurPointCursor(self,CurrentPointCursor_obj):
    #     self.cursor = CurrentPointCursor_obj
    # def showCursor(self):
    #
    #     for srcN,pm in self.xyPlotManagersDict.items():
    #         print pm.plot_dict_list



