import time,sys,os,traceback
import roundto
class Logger(object):
    def __init__(self, fileprefix = None, path = None,header = "", filename = None, maxValue = None, minValue = None, skipRepetitions = True, testmode = False):
        self.prevData = None
        self.skipRepetitions = skipRepetitions
        self.testmode = testmode
        self.header = header
        self.maxValue = maxValue
        self.minValue = minValue
        if self.header and not self.header.endswith("\n"):
            self.header+="\n"
        self.file = None
        self.t0 = None
        self.tprev = None
        if not filename:
            if fileprefix:
                fname = fileprefix
            else:  #use script basename as log file name
                fname = sys.argv[0]
                _,fname = os.path.split(fname)
                idx = fname.rfind('.')
                if idx > 1:
                    fname = fname[0:idx] #remove extension if there is one
            date = time.strftime("%Y%m%d_",time.localtime())
            self.filename = date+fname+".dat"
        if path and not os.path.isabs(self.filename) and os.path.exists(path):
            self.filename = path + os.sep + self.filename
        else:
            self.filename = os.path.dirname(sys.argv[0])+os.sep+self.filename
        print("logging to",self.filename)
    def appendData(self,*data):
        if data:
            if self.skipRepetitions:
                if self.prevData:
                    if len(data)==len(self.prevData):
                        for new,old in zip(data,self.prevData):
                            if new!=old:
                                self._appendData(data)
                                break
                    else:
                        self._appendData(data)
            else:
                self._appendData(data)
            self.prevData = data

    def _appendData(self,data):
        if self.minValue:
            for el in data:
                if el is None or el<self.minValue:
                    return
        if self.maxValue:
            for el in data:
                if el is None or el>self.maxValue:
                    return

        try:
            if self.file is None:
                if not self.testmode:
                    if os.path.exists(self.filename):
                        newfile = False
                    else:
                        newfile = True
                    self.file = open(self.filename,"a+")
                    if newfile: #we are writing header only once
                        self.file.write(self.header)
                else:
                    self.file = False
                if self.skipRepetitions:
                    self._appendData(self.prevData)
            t = time.time()
            if not self.t0:
                self.t0 = t
            t = t - self.t0
            if abs(t) < 1e-3:
                tr = 0
            else:
                tr = roundto.roundTo(t,t-self.tPrev)
            self.tPrev = t
            msg = str(tr)+"\t"+"\t".join([str(el) for el in data])+"\n"
            if self.testmode:
                print("new data received:", msg,end = "")
            if self.file:
                self.file.write(msg)
        except:
            traceback.print_exc()
            self.file = False
    def __del__(self):
        self.close()
    def close(self):
        if self.file and not self.file.closed:
            self.file.close()



if __name__ == "__main__":
    test_seq = [1.0,1.0,1.1,1.3,1.3,1.4]
    log = Logger()
    log = Logger(fileprefix="SomePrefix",testmode=False,minValue=1.05,
                 header="#first (a.u)\tsecond (foo)")
    for t in test_seq:
        log.appendData(t)

