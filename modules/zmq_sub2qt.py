from __future__ import print_function
from qtpy import QtCore,QtWidgets
import zmq,traceback
class Zmq2Qt(QtCore.QThread):
    sigGotMessage = QtCore.Signal(object)

    def bind(self, port):
        addr = "tcp://*:{}".format(port)
        try:
            self.socket.bind(addr)
            self.go = True
            self.start()
        except:
            traceback.print_exc()


    def __init__(self,context,port,verbose = False):
        self.verbose = verbose
        super(Zmq2Qt,self).__init__()
        # self.context = zmq.Context()
        self.context = context
        self.socket = context.socket(zmq.SUB)
        self.socket.setsockopt(zmq.SUBSCRIBE, b"")
        self.bind(port)
    def run(self):
        while self.go:
            try:
                rcv = self.socket.recv_pyobj()
                self.sigGotMessage.emit(rcv)
                if self.verbose:
                    self.printf(rcv)
            except:
                traceback.print_exc()
    def stop(self):
        self.go = False
    def printf(self,obj):
        print(obj)

if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    context = zmq.Context()
    rcv = Zmq2Qt(context,6007)
    rcv.start()
    w = QtWidgets.QMainWindow()
    w.show()
    rcv.sigGotMessage.connect(rcv.printf)
    app.exec_()
    rcv.stop()