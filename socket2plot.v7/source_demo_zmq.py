from __future__ import print_function
import zmq
from random import choice
import numpy as np
import time
PUB_ADDRESS = "tcp://127.0.0.1:5002"
context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind(PUB_ADDRESS)


i=0
print('Transmitting at {}'.format(PUB_ADDRESS))
while True:
    ch1 = (np.random.random()-0.5)*2.001
    ch2 = (np.random.random()-0.5)*2.001
    #t = int(np.modf(time.time()/1000)[0]*1e6)
    t = time.time()
    #msg = '%d %0.3f %s, %f, %e' %(i,t,'some text',ch1,ch2)
    mp = (t, ch1, ch2, i)
    #print "->",msg
    #socket.bind("tcp://127.0.0.1:4999")
    #time.sleep(0.1)
    #socket.send( msg )
    #socket.unbind("tcp://127.0.0.1:4999")
    socket.send_pyobj(mp)
    #socket.send_multipart(mp)
    time.sleep(0.1)
    i = i+1

