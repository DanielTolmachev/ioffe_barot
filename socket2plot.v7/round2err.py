from numpy import floor,log10


def round_to_err(x, err):
    if err == 0:
        return x
    else:
        return round(x, -int(floor(log10(err)) - 2))