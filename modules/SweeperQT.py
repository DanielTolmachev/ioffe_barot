from qtpy import QtCore
class SweeperQT(QtCore.QObject):
    sigNewValue = QtCore.Signal(float)
    sigSweeperStopped = QtCore.Signal()
    def __init__(self):
        """
        call sweep to sweep value in range of values,
          emitting sigNewValue on each new value
        """
        super(SweeperQT,self).__init__()
        self.timer = None
        self.val_current = None
        self.getCurrentValue = None
    def sweep(self,val_min, val_max, step, dt=0.1, resume=True, loop = False, bidirectional = False ,current_value = None):
        if not self.timer:
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.nextValue)
        self.timer.stop()
        self.timer.setInterval(dt*1000)
        if val_min>val_max:
            val_min,val_max = val_max,val_min
        self.val_min = val_min
        self.val_max = val_max
        self.step = step
        if loop == True:
            self.loop = -1
        elif loop == False:
            self.loop = 1
        self.bidirectional = int(bidirectional)
        self.step_cnt = 0
        if resume and (current_value or self.getCurrentValue): # continue from current value
            if current_value:
                self.val_current = current_value
            elif self.getCurrentValue:
                self.val_current = self.getCurrentValue()
        elif step>=0: # sweeping up, set minimal value
            self.val_current = val_min
        else: # sweeping down, set maximal value
            self.val_current = val_max
        self.sigNewValue.emit(self.val_current)
        self.timer.start()
    def nextValue(self):
        self.step_cnt += 1
        self.val_current = self.val_current + self.step
        if self.val_min >  self.val_current or self.val_current > self.val_max: #end of loop
            #TODO make comparison to ignore almost equity
            if self.bidirectional==1: # continue sweep in opposite direction
                self.bidirectional = 2
                self.step = -self.step
            else:
                if self.bidirectional == 2: #change direction, and end single sweep
                    self.bidirectional = 1
                    self.step = -self.step
                self.loop-=1
                if self.loop!=0:
                    if self.step>=0:
                        self.val_current = self.val_min
                    else:
                        self.val_current = self.val_max
        if self.loop==0: #stop sweeping
            if self.val_current > self.val_max:
                self.val_current = self.val_max
            elif self.val_current< self.val_min:
                self.val_current = self.val_min
            self.timer.stop()
            self.sigSweeperStopped.emit()
        self.sigNewValue.emit(self.val_current)

    def stop_sweep(self):
        if self.timer.isActive():
            self.timer.stop()
            self.sigSweeperStopped.emit()