#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      root
#
# Created:     13-05-2015
# Copyright:   (c) root 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from __future__ import print_function
def detect_significant_order_from_string(s):
    dot = s.find('.')
    zeros = 0
    pos = -1
    for i,c in enumerate(reversed(s)):
        if c=='0':
            zeros+=1
        elif c!='.':
            break
    if dot>-1:
        precision = (len(s)-1-dot)
    else:
        precision = 0
    return {'precision':precision,'zeros':zeros}

if __name__ == '__main__':
    test = ['100.0' , '0.025' , '100.25' , '10.0400', ' 1000', '100.']
    for t in test:
        print(t,' ',detect_significant_order_from_string(t))
