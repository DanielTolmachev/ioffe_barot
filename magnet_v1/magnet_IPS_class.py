import sys
sys.path.append( '../modules')
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev"

MAXRATE = .4 / 60.  #Tesla/seconds
MAXFIELD = 7 #Tesla
FIELDS = [4,5,6]   # fields when we need to lower rate
RATES = [.3/60., .25/60., .15/60.] #maximal allowed rate for fields


LAN = ['socket','ip','lan','ethernet']
COM = ['com','serial','rs232','rs-232']
INTERFACES = [LAN,COM]


class magnet_IPS(object):
    proxy = None
    opened = False
    idn = ''
    def __init__(self,interface_type,dev_address):
        self._read = self._write = self._open = self.close = self.dummy
        self.__init__interface__(interface_type,dev_address)
    def __init__interface__(self,interface_type,dev_address):
        self.dev_address = dev_address
        self.interface_type = interface_type
        if type(interface_type)==str:
            if interface_type.lower() in LAN:
                from instr_socket import Instr_Socket
                interface_type = Instr_Socket
            elif interface_type.lower() in COM:
                import serial
                self.interface = serial.Serial()
                self.interface.port = dev_address
                self.interface.timeout = 1
                self._write = self.interface.write
                self._read = self.interface.readline
                self.close = self.interface.close
                self._open = self.interface.open
                self.setRemote()
                self.setLF()
                #self.setSlow()
            else:
                self._wrongInterface_msg()
                return
        else:
            self._wrongInterface_msg()
            return
        if hasattr(interface_type,'read') and \
                hasattr(interface_type,'write') and\
                hasattr(interface_type,'open'):
            if not self.proxy:
                self.proxy = interface_type(dev_address)
                self._read = self.proxy.read
                self._write = self.proxy.write
                self.open = self.proxy.open
                self.close = self.proxy.close
        #self.open(dev_address)
    def _wrongInterface_msg(self):
        print('magnet class support these types of interfaces')
        for i in INTERFACES:
            print(i)
    def setAddress(self,dev_address):
        self.__init__interface__(self.interface_type,dev_address)
    def setLF(self):
        return self.ask(b'Q2\r')
    def dummy(self,*args):
        print('empty function called with',args)
    def ask(self,query):
        self.write(query)
        return self.read()
    def read(self):
        if self.interface.isOpen():
            return self._read()
    def write(self,msg):
        if self.interface.isOpen():
            return self._write(msg)
    def open(self):
        self._open()
    # def setqueryFreq(self,freq):
    #     self.setFreq(freq)
    #     return self.queryFreq()
    # def setField(self,field):
    #     return self.write('freq%g\n'%freq)
    # def queryFreq(self):
    #     f = self.ask('freq?\n')
    #     try:
    #         return float(f)
    #     except:
    #         print sys.exc_info()
    #         return f
    # def setPower(self,pwr):
    #     return self.write('AMPR%g\n'%pwr)
    # def queryPower(self):
    #     p = self.ask('AMPR?\n')
    #     try:
    #         return float(p)
    #     except:
    #         print sys.exc_info()
    #         return p
    def getID(self):
        if not self.idn:
            self.idn = self.ask(b'V\r')
        return self.idn
    def setRemote(self):
        return self.ask(b'C3\r') # C3 = Remote & Unlocked
    def setSlow(self):
        return self.ask(b'M5\r') # M5  Tesla, Slow
    def setField(self,field):
        if field>MAXFIELD:
            field = MAXFIELD
        self.setFieldTarget(field)
        rate = MAXRATE
        for i in reversed(range(0, len(FIELDS))):
            if field > FIELDS[i]:
                rate = RATES[i]
                print ('rate reduced to {} T/min'.format(rate*60))
                break
        self.setFieldRate(MAXRATE)
        self.go()
    def go(self):
        return self.ask(b'A1\r')
    def stop(self):
        return self.ask(b'A0\r')
    def gotoZero(self):
        return self.ask(b'A2\r')
    def queryField(self):
        ret = self.ask(b'R7\r')
        if ret and ret[0] == b'R':
            res = float(ret[2:].strip())
            # if res>10:
            #     print ret
            return res
    def queryFieldRate(self):
        ret = self.ask(b'R9\r')
        if ret[0] == b'R':
            return float(ret[2:].strip())/60 #convert T/min to T/s
    def queryFieldTarget(self):
        ret = self.ask(b'R8\r')
        if ret and ret[0] == b'R':
            return float(ret[2:].strip())
    def stat(self):
        return self.ask(b'X\r')
    def setFieldTarget(self,field):
        ret = self.ask(b'J{}\r'.format(field))
        return ret
    def setFieldRate(self,rate):
        if rate>MAXRATE:
            rate = MAXRATE
        ret = self.ask(b'T{}\r'.format(rate*60)) #convert T/s to T/min
        return ret
if __name__ == '__main__':
    mg = magnet_IPS('serial','COM8')
    print( mg.getID())
