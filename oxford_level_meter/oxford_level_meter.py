import sys,os,time,traceback,mmap,datetime
sys.path.append("../modules")
import enable_exception_logging
import install_exception_hook
import oxford_level_meter
import quickQtApp,zmq_sub2qt,options

from qtpy import QtWidgets,QtCore,QtGui
from collections import OrderedDict

SCRIPT = 'console_logger.py'

def check_if_running():
    pid = port = None
    mmap_file = mmap.mmap(-1,1024,tagname="oxford_cryogen_level_logger",access=mmap.ACCESS_WRITE)
    line = mmap_file.readline().strip(b"\0")
    if line:
        lines = line.split()

        for l in lines:
            try:
                if b"PID" in l:
                    pid = int(l[3:])
                elif b"PORT" in l:
                    port = int(l[4:])
            except:
                pass
        if pid and port:
            print("process {} is already running at port {}".format(pid,port))
        else:
            print("process is already running {}".format(line))
        return pid,port
    return pid,port

class CentralWidget(QtWidgets.QWidget):
    sigServiceStarted = QtCore.Signal(object)
    sigServiceRunning = QtCore.Signal(object)
    sigDisplayMessage = QtCore.Signal(object)
    sigInfoModified = QtCore.Signal()
    def __init__(self):
        super().__init__()
        self.sub = None
        self.gl = QtWidgets.QGridLayout()
        self.setLayout(self.gl)

        self.hl_labels = QtWidgets.QHBoxLayout()
        self.labels = []
        self.gl.addLayout(self.hl_labels, 0, 0)
        self.te = QtWidgets.QTextEdit()
        self.te.setReadOnly(True)
        self.te.setMinimumHeight(self.te.fontMetrics().height()*5)
        p = self.te.palette()
        p.setColor(QtGui.QPalette.Base, self.palette().color(QtGui.QPalette.Background))
        self.te.setFrameStyle(0)
        self.te.setPalette(p)
        self.te1 = QtWidgets.QTextEdit()
        self.te1.setReadOnly(True)
        self.te1.hide()
        self.gl.addWidget(self.te)
        self.gl.addWidget(self.te1)
        self.timer = QtCore.QTimer()
        self.info = OrderedDict()
        self.timer.setSingleShot(True)
        self.sigServiceRunning.connect(self.subscribe)
        self.sigDisplayMessage.connect(self.displayData)
        self.sigInfoModified.connect(self.displayInfo)
        self.options = options.getGlobalOptionsDict()
        
        self.value_names = self.options.get("Instrument.level_names",
                                            ["Helium","Nitrogen"])
        self.evaporation_rates = self.options.get("Instrument.level_rates",
                                                  [17,57])
        
        self.start_service_if_needed()

    def start_service_if_needed(self):
        pid, port = check_if_running()
        if port:
            msg ="service is running procid {} on port {}".format(pid,port)
            self.sigDisplayMessage.emit("")
            self.info["status"] = msg
            self.sigInfoModified.emit()
            self.sigServiceRunning.emit((pid, port))
        if not port:
            try:
                if sys.platform.startswith('win'):
                    pid = os.spawnl(os.P_NOWAIT, sys.executable, '"' + sys.executable + '"', '"' + SCRIPT + '"')
                else:
                    pid = os.spawnl(os.P_NOWAIT, sys.executable, sys.executable, SCRIPT)
                msg = "starting {} procid {}".format(SCRIPT, pid)
                self.sigDisplayMessage.emit(msg)
                print(msg)
            except:
                traceback.print_exc()
            self.timer.timeout.connect(self.start_service_if_needed)
            self.timer.start(1000)

        return pid, port
    def subscribe(self,pid_port):
        pid,port = pid_port
        print("connecting to process {} on port {}".format(pid,port))
        if self.sub:
            self.sub.stop()
        self.sub = zmq_sub2qt.Zmq2Qt(connect_address = ("127.0.0.1",port))
        self.sub.sigGotMessage.connect(self.displayData)
        self.sub.start()
    def displayData(self,args):
        if type(args)==tuple and self.sender() == self.sub:
            while len(args)>len(self.labels):
                l = QtWidgets.QLabel()
                l.setSizePolicy(QtWidgets.QSizePolicy.Expanding,
                               QtWidgets.QSizePolicy.Expanding)
                f = l.font()
                f.setPointSizeF(f.pointSizeF()*options.getGlobalOptionsDict().get(
                    "GUI.MainLabels.Font.Size.Scale",2))
                l.setFont(f)
                self.hl_labels.addWidget(l)
                self.labels.append(l)
            for i,val in enumerate(args):
                if val is None:
                    s = " - "
                else:
                    s = str(val)
                self.labels[i].setText(s)
            self.calculateEstimation(args)
        else:
            if args=="" and "last message" in self.info:
                del self.info["last message"]
            else:
                self.info["last message"] = str(args)
                self.sigInfoModified.emit()
    def displayInfo(self):
        self.te.clear()
        for key,val in self.info.items():
            if val is not None:
                self.te.append("<b>{}</b>:{}".format(key,val))
    def calculateEstimation(self,values):
        for i,v in enumerate(values):                        
            if i<len(self.value_names):
                val_name = self.value_names[i]
            else:
                val_name = "level%d" % i 
            if i<len(self.evaporation_rates) and v is not None:
                rate = self.evaporation_rates[i]
                est = datetime.timedelta(days = abs(v/rate))
                today = datetime.datetime.now()
                end = today+est
                est_s = ""
                if est.days:
                    est_s += "%d day" % est.days
                    if abs(est.days)>1:
                        est_s += "s"
                hours = est.seconds//3600
                minutes = (est.seconds % 3600) //60
                if hours:
                    est_s += " %d hour" % hours
                    if abs(hours)>1:
                        est_s += "s"
                if minutes:
                    est_s += " %d minute" % minutes
                    if abs(minutes)>1:
                        est_s += "s"
                if today.day == end.day:
                    end_s = end.strftime("today %H:%M")
                elif today.day+1 == end.day:
                    end_s = end.strftime("tomorrow %H:%M")
                else:
                    end_s = end.strftime("%A %H:%M")
                if est_s:
                    self.info[val_name] = "will evaporate in {} ({})".format(
                        est_s,end_s)
                elif val_name in self.info: del self.info[val_name]
            elif val_name in self.info: del self.info[val_name]
        if self.evaporation_rates:
            self.sigInfoModified.emit()


if __name__ == "__main__":
    app, opt, win, cons = quickQtApp.mkAppWin("cryogen level meter")
    cw = CentralWidget()
    win.setCentralWidget(cw)
    win.show()
    app.exec()
    opt.savetodisk()
