from plugins.instr_plugin_base import Instr_Plugin_Base
import zmq,time,struct,sys

DEV_ADDRESS = "127.0.0.1"
DEV_PORT = 5020

class Unipan(Instr_Plugin_Base):
    __instrument_info__ = {
        "channels2rec": 2,
        "timechannel": 0,
        "name": 'Unipan',
        "comment": 'счет фотонов, конструктор Пак',
        "channels_names": ['Time', "СH0","CH1"],
        "channels_units": ['s', "counts","counts"]}
    def __init__(self,*args,**kwargs):
        super(Unipan,self).__init__(*args,**kwargs)
        self.context = zmq.Context()
        self.loadattr("address",DEV_ADDRESS)
        self.loadattr("port",DEV_PORT)

    def run(self):
        self.socket = self.context.socket(zmq.SUB)
        self.socket.setsockopt(zmq.SUBSCRIBE, b"")
        self.socket.connect("tcp://{}:{}".format(self.address,self.port))
        while self._running:
            data = self.socket.recv()
            # lw = self.dev.getLineWidth_nm
            try:
                if len(data)>0:
                    c1,c2 = struct.unpack("xxxxII",data[:12])
                    self.updated.emit((self._id,time.time(),c1,c2))
            except:
                self.updated.emit((self._id, time.time(), "received {} bytes, {}".format(len(data), sys.exc_info()[0])))
__pluginclass__ = [Unipan]