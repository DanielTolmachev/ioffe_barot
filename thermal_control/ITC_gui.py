#coding:utf-8-------------------------------------------------------------------
# Name:        	Temperature controller
#		ITC_gui.py
#
# Purpose:	This app allow remote read of temperature from ITC temperature controller from Oxford Instruments
#		and broadcast temperature value using 
#		ZMQ messaging library (PUB pattern)
#		
# Made for:	e3.physik.tu-dortmund.de
#
# Created:  	2017
#-------------------------------------------------------------------------------
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@gmail.com/Daniel.Tolmachev@mail.ioffe.ru)"

DEV_ADDRESS = "129.217.155.3"
DEV_PORT = 1234
DEV_GPIB_ADDRESS = 24
DEF_UPDATE_INTERVAL = 1000 #ms

import sys,time
sys.path.append("../modules")
import install_exception_hook
import ITC_device_class
import quickQtApp,QMenuFromDict
from qtpy import QtWidgets,QtCore
import logger
import zmq_annotated_publisher,device_address_widget

app,opt,win,cons = quickQtApp.mkAppWin("Temperature",icon = "temperature.png")
dev_address = DEV_ADDRESS
dev_port = DEV_PORT
dev_gpib_address = DEV_GPIB_ADDRESS
update_interval = DEF_UPDATE_INTERVAL
publish_address = opt.get("pub.address",u'tcp://*:5019')
pub = zmq_annotated_publisher.Zmq_Pub_Annotated(address=publish_address,
                                                fieldnames=["time","Temperature"],
                                                fieldunits=["s","K"],
                                                name="Temperature",
                                                description="ITC 502 Temperature controller by Oxford Instruments")

itc = ITC_device_class.ITC((DEV_ADDRESS, DEV_PORT), gpib_address=DEV_GPIB_ADDRESS)
sensor = opt.get("hw.sensor",1)
tlog = logger.Logger(fileprefix=opt.get("log.fileprefix","ITC"),
                     path= opt.get("log.path","."),
                       header="#time (s)\tTemperature (K)",
                     maxValue=opt.get("log.maxTempToLog",288.))

conn_status = QtWidgets.QLabel()
win.statusBar().addWidget(conn_status)


w_addr = device_address_widget.DeviceAddressWidget(itc.address,
                                                   interfaces=["lan","serial"],
                                                   default_ip_port=DEV_PORT,
                                                   device_object = itc)
w_addr.sigConnectionStatus.connect(conn_status.setText)
QMenuFromDict.AddMenu(win.menuBar(),[
                                    ("device",[
                                                ("connect",w_addr.open),
                                                ("disconnect",w_addr.close),
                                                ("change address",w_addr.show)
                                                ]),
                                    ])


def update():
    t = time.time()
    T = itc.getTemperature(sensor)
    tlog.appendData(T)
    lbl_temp.setText(str(T)+" K")
    pub.send_pyobj((t,T))
def label_resize(evt):
    global lbl_size,lbl_temp_hr,lbl_temp_wr
    w = lbl_temp.width()
    h = lbl_temp.height()
    r = min(w*lbl_temp_wr,h*lbl_temp_hr)
    print(w, h, r)
    if r!=lbl_size:
        lbl_font.setPointSize(r)
        lbl_temp.setFont(lbl_font)
        lbl_size = r

cw = QtWidgets.QWidget()
gl = QtWidgets.QGridLayout()
cw.setLayout(gl)
lbl_temp = QtWidgets.QLabel()
lbl_temp.setSizePolicy(QtWidgets.QSizePolicy.Ignored,QtWidgets.QSizePolicy.Ignored)
fm = lbl_temp.fontMetrics()
lbl_font = lbl_temp.font()
lbl_temp_wr = lbl_font.pointSize()/fm.width("288.8 K")
lbl_temp_hr = lbl_font.pointSize()/fm.height()
lbl_font.setPointSize(lbl_font.pointSize() * 2)
lbl_temp.setFont(lbl_font)
lbl_size = 0
lbl_temp.resizeEvent = label_resize
gl.addWidget(lbl_temp)
win.setCentralWidget(cw)
win.show()
tm = QtCore.QTimer()
tm.setInterval(update_interval)
tm.timeout.connect(update)
tm.start()
w_addr.open() #open device and update statusbar
app.exec()
opt.savetodisk()