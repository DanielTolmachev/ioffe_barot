from __future__ import print_function
from numpy import floor,round,log10
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
#from pyqtgraph.Point import Point



class RangeDisplay(QtCore.QObject):

    rangeChanged = QtCore.pyqtSignal(tuple)
    def __init__(self,pen={"color":"888","style":2,"width":2}):
        super(RangeDisplay,self).__init__()
        self.pen = pen
        self.borderLeft = 0
        self.borderRight = 1
        self.plots = []
        self.linesLeft = []
        self.linesRight = []
        self.movable = False
        self.visible = False
        self.PRECISIONPIXEL = 10 #we move scale with 10 pixel precision
        self.PRECISIONMIN = 100 # but precision should not be less then scale/PRECISIONMIN
    def addPlot(self,*arg):
        if arg:
            for a in arg:
                if a not in self.plots:
                    self.plots.append(a)
                    self.linesLeft.append(pg.InfiniteLine(angle=90,pen = self.pen))
                    self.linesRight.append(pg.InfiniteLine(angle=90, pen = self.pen))
                    a.addItem(self.linesLeft[-1])
                    a.addItem(self.linesRight[-1])
                    self.linesLeft[-1].sigPositionChanged.connect(self.onLeftBorderMoved)
                    self.linesRight[-1].sigPositionChanged.connect(self.onRightBorderMoved)
                    self.linesLeft[-1].setVisible(self.visible)
                    self.linesRight[-1].setVisible(self.visible)
            self._setMovable()
            self._moveLines()
    def setMovable(self,state):
        self.movable = bool(state)
        self._setMovable()
    def _setMovable(self):
        for l,r in zip(self.linesLeft,self.linesRight):
            l.setMovable(self.movable)
            r.setMovable(self.movable)

    def setVisible(self,state):
        if state:
            self.visible = True
            for l,r in zip(self.linesLeft,self.linesRight):
                l.setVisible(True)
                r.setVisible(True)
                l.setPos(self.borderLeft)
                r.setPos(self.borderRight)
        else:
            self.visible = False
            for l,r in zip(self.linesLeft,self.linesRight):
                l.setVisible(False)
                r.setVisible(False)
    def setRange(self,left,right):
        #self.blockSignals(True)
        if left!=self.borderLeft:
            self.borderLeft = left
            self._moveLeft()
        if right!=self.borderRight:
            self.borderRight = right
            self._moveRight()
        #self.blockSignals(False)
    def getRange(self):
        return self.borderLeft,self.borderRight
    def _moveLeft(self):
        for l in self.linesLeft:
            l.blockSignals(True)
            l.setPos(self.borderLeft)
            l.blockSignals(False)
    def _moveRight(self):
        for r in self.linesRight:
            r.blockSignals(True)
            r.setPos(self.borderRight)
            r.blockSignals(False)
    def _moveLines(self):
        for l,r in zip(self.linesLeft,self.linesRight):
            l.setPos(self.borderLeft)
            r.setPos(self.borderRight)
    def round_x(self,line):
        x = line.x()
        vb = line.getViewBox()
        range = vb.viewRange()[0][1]-vb.viewRange()[0][0]
        pixel_precision = range/vb.boundingRect().width()
        prec_needed = min(range/self.PRECISIONMIN,pixel_precision*self.PRECISIONPIXEL)
        order = 10**floor(log10(prec_needed))
        x = round(x/order)*order
        print(range,pixel_precision,prec_needed,order,x)
        return x
    def onLeftBorderMoved(self,line):
        if self.visible:
            x = self.round_x(line)
            if self.borderLeft!=x:
                self.borderLeft = x
                self.rangeChanged.emit((self.borderLeft,self.borderRight))
                self._moveLeft()
            else:
                line.setPos(x)
    def onRightBorderMoved(self,line):
        #print "Rangedisplay.onRightBorderMoved invoked"
        if self.visible:
            x = self.round_x(line)
            if self.borderRight!=x:
                self.borderRight = x
                self.rangeChanged.emit((self.borderLeft,self.borderRight))
                self._moveRight()
            else:
                line.setPos(x)


## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    from numpy.random import random
    #generate layout
    app = QtGui.QApplication([])
    win = pg.GraphicsWindow()
    win.setWindowTitle('pyqtgraph example: crosshair')
    label = pg.LabelItem(justify='right')
    win.addItem(label)
    p1 = win.addPlot(row=1, col=0)
    p2 = win.addPlot(row=2, col=0)

##    region = pg.LinearRegionItem()
##    region.setZValue(10)
    # Add the LinearRegionItem to the ViewBox, but tell the ViewBox to exclude this
    # item when doing auto-range calculations.
##    p2.addItem(region, ignoreBounds=True)

    #pg.dbg()
    p1.setAutoVisible(y=True)


    #create numpy arrays
    #make the numbers large to show that the xrange shows data from 10000 to all the way 0
    data1 = 10000 + 15000 * pg.gaussianFilter(random(size=10000), 10) + 3000 * random(size=10000)
    data2 = 15000 + 15000 * pg.gaussianFilter(random(size=10000), 10) + 3000 * random(size=10000)

    p1.plot(data1, pen="r")
    p1.plot(data2, pen="g")

    p2.plot(data1, pen="w")

    def update():
        region.setZValue(10)
        minX, maxX = region.getRegion()
        p1.setXRange(minX, maxX, padding=0)

##    region.sigRegionChanged.connect(update)

    def updateRegion(window, viewRange):
        rgn = viewRange[0]
        region.setRegion(rgn)




    r = RangeDisplay()
    r.addPlot(p1,p2)
    r.setRange(1100,1800)
    r.setMovable(True)
    r.addPlot(p2)
    r.setVisible(True)
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
