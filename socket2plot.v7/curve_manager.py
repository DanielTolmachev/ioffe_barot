#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Daniel
#
# Created:     28.05.2015
# Copyright:   (c) Daniel 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from __future__ import print_function

import traceback

from numpy import mod
import pyqtgraph as pg

MODE_REGULAR = 0
MODE_LIVE = 1
MODE_XY = 2

def_colors = ['w','y','g','r']
class CurveManager(object):
    livemodeReady = False
    plotManager = None
    mode_regular = False
    mode_xy = False
    mode = MODE_REGULAR
    hide_plot_src_number = None
    autoCreatePlots = False
    showEmpyPlot = False
    def __init__(self,sources_list,colors_list = def_colors):
        self.srcl = sources_list
        self.colors = colors_list
        self.datalist = []
        self.datalist_xy = []
        self.plotWidgets = set()
        #self.xymode_dock_visibility = [[] for i in range(0,len.srcl)]
        # curvelist = []
        # crv = 0
        # for src in self.srcl:
        #     if src.enabled:
        #         for i,k in enumerate(src.channels2show):
        #             c = np.mod(crv,len(self.colors))
        #             curvelist.append(pg.PlotCurveItem(pen=self.colors[c]))
        #             #print 'curve is added, color is {}'.format(colors[c])
        #             #self.pw.addItem(curvelist[-1])
        #             crv+=1
        # self.datalist.append(curvelist)
        # print "created curve manager",self
        # print "self.plotManager = ",self.plotManager
    def _prepareLiveMode(self):
        """
        This should be called before setupplot_live
        """
        self.livemode_list = [[] for _ in self.srcl]
        self.livemode_list_docks = [None for _ in self.srcl]
        self.livemode_list_dock_hidden = [False for _ in self.srcl]
        print(self.livemode_list)
        crv = 0
        srcn = 0
        for i,src in enumerate(self.srcl):
            if src.enabled:
                for k,chan in enumerate(src.channels2show):
                    c = mod(crv,len(self.colors))
                    self.livemode_list[i].append(pg.PlotCurveItem(pen=self.colors[c]))
                    #print 'curve is added, color is {}'.format(colors[c])
                    #self.plotWidget.addItem(curvelist[-1])
                    crv+=1
                srcn+=1
        print('preparing live mode: sources {}, curves {}'.format(srcn,crv))
        self.setupplot_live()
        self.livemodeReady = True
    def setupplot_live(self):
        """
        This creates docks, each containing one plot. For Live mode
        """
        if self.plotManager:
            #self.plotWidget.
            #pg.PlotWidget.add
            #self.plotManager.saveState(MODE_LIVE)
            self.plotManager.setMode(MODE_LIVE)
            print('livemode_list: ')
            for i,l in enumerate(self.livemode_list):
                print(l)
                if l:
                    d,p = self.plotManager.createDock(self.srcl[i].name)
                    self.livemode_list_docks[i] = d
                    if self.autoCreatePlots:
                        d.hide()
                        self.livemode_list_dock_hidden[i] = True
                    for j in p.items(): #clear plot if needed
                        p.removeItem(j)
                    for crv in l:
                        p.addItem(crv)
            # self.plotManager.loadState(MODE_REGULAR)
    def livemode(self,src_index,chan_index):  # returns pyqtgraph curve object
        if not self.livemodeReady:
            self._prepareLiveMode()
        if self.mode!=MODE_LIVE:
            self.setup_mode(MODE_LIVE)
        if self.livemode_list_dock_hidden[src_index]:
            self.livemode_list_dock_hidden[src_index] = False
            print("Curve manager: show plot for {}".format(src_index))
            self.livemode_list_docks[src_index].show()
        #print 'dbg:live mode curves total', self.livemode_crv_count()
        return self.livemode_list[src_index][chan_index]
    def livemode_crv_count(self):
        sz = 0
        for i in self.livemode_list:
            sz+=len(i)
        return sz
    def clear_live_mode(self):
        if self.livemodeReady:
            for l in self.livemode_list:
                if l:
                    for crv in l:
                        #crv.clear()
                        crv.setData([])
                        #self.plotWidget.removeItem(crv)
            self.livemodeReady = False
    def new(self,showEmptyPlot = False):
        self.datalist.append([])
        self.datalist_xy.append([])
        self.mode = None
        self.showEmpyPlot = False
    def setupplot_regular(self):
        if self.plotManager:
            curvelist = []
            src_list = []
            #self.plotManager.saveState(MODE_REGULAR)
            self.plotManager.setMode(MODE_REGULAR)
            crv = 0
            for i,src in enumerate(self.srcl):
                if src.enabled:
                    curvelist.append([])
                    d,p = self.plotManager.createDock(self.srcl[i].name)
                    #d.hide()
                    for i,k in enumerate(src.channels2show):
                        c = mod(crv,len(self.colors))
                        ci = pg.PlotCurveItem(pen=self.colors[c])
                        curvelist[-1].append(ci)
                        #ci.mousePressEvent = self.onCurvePress
                        #print 'curve is added, color is {}'.format(colors[c])
                        p.addItem(ci)
                        crv+=1
                    self.plotWidgets.add(p)
                else:
                    curvelist.append(None)
            if self.datalist:
                self.datalist[-1] = curvelist
            else:
                self.datalist.append(curvelist)
            print("setup plot for timemode {}x{}".format(len(self.datalist),crv))
            #self.setup_mode(MODE_REGULAR)
    def onCurvePress(self,args):
        print(args)
    def curve_regularmode(self,src_index,chan_index, record_index = -1):
        if not self.datalist:
            self.datalist.append([])
        if not self.datalist[-1]:
            self.setupplot_regular()
        if self.mode!=MODE_REGULAR:
            self.setup_mode(MODE_REGULAR)
        return self.datalist[record_index][src_index][chan_index]
    def hide_regular_mode(self):
        cnt = 0
        self.plotManager.clear()
        for single_record in self.datalist:
            for source in single_record:
                if source:
                    for crv in source:

                        # self.plotManager.removeItem(crv)
                        cnt += 1
        print('removing curves ({})'.format(cnt))
        self.mode_regular = False
    def removeAllCurves(self):
        for p in self.plotWidgets:
            for it in p.items():
                if type(it)== pg.PlotCurveItem:
                    p.removeItem(it)
                    self.datalist = []
    def clear_regular_mode(self):
        self.hide_regular_mode()
        self.datalist = []
    def show_regular_mode(self):
        print('Switching to regular mode', end=' ')
        #self.hide_xy()
        cnt = 0
        for single_record in self.datalist:
            for source in single_record:
                if source:
                    for c in source:
                        self.plotManager.addItem(c)
                        cnt += 1
        print(' adding {} curves'.format(cnt))
        self.mode_regular = True
    def setupplot_xy(self):
        if self.plotManager:
            curvelist = []
            src_list = []
            #self.plotManager.saveState(MODE_XY)
            self.plotManager.setMode(MODE_XY)
            self.xy_dock_visibility =  [False for _ in range(len(self.srcl))]
            self.xy_docklist = [None for _ in range(len(self.srcl))]
            crv = 0
            print("setupplot_xy srcX =",self.srcX)
            print(self.plotManager)
            for srcN,src in enumerate(self.srcl):
                #if src.enabled and n!=self.srcX:
                if src.enabled:
                    curvelist.append([])
                    d,p = self.plotManager.createDock(src.name)
                    self.xy_docklist[srcN] = d
                    if not self.showEmpyPlot:
                        d.hide() #Dock are created hidden
                        self.xy_dock_visibility[srcN] = False
                    else:
                        self.xy_dock_visibility[srcN] = True
                    for i,k in enumerate(src.channels2show):
                        c = mod(crv,len(self.colors))
                        ci = pg.PlotCurveItem(pen=self.colors[c])
                        curvelist[-1].append(ci)
                        #print 'curve is added, color is {}'.format(colors[c])
                        p.addItem(ci)
                        crv+=1
                    self.plotWidgets.add(p)
                else:
                    curvelist.append(None)
            self.datalist_xy[-1] = curvelist
            #self.setup_mode(MODE_XY)
        else:
            print(self.plotManager)
    def setX(self,source_number_to_use_as_x):
        self.srcX = source_number_to_use_as_x
    def curve_xy(self,src_index,chan_index):
        if not self.datalist_xy:
            self.datalist_xy.append([])
        if not self.datalist_xy[-1]:
            self.setupplot_xy()
        if self.mode!=MODE_XY:
            self.setup_mode(MODE_XY)
        try:
            d = self.datalist_xy[-1]
            d1 = d[src_index]
            crv = d1[chan_index]
            #print self.plotManager.dock_dict_list[MODE_XY]
            #self.plotManager.docks[self.srcl[src_index].name].show()
            self.showDock(MODE_XY,src_index)
            return crv
        except:
            print("curve_xy",src_index,self.srcl[src_index].name,chan_index)
            traceback.print_exc()
    def showDock(self,mode,src_index):
        # name = self.srcl[src_index].name
        if not self.xy_dock_visibility[src_index]:
            # print 'show dock',src_index,self.xy_dock_visibility[src_index]
            self.xy_docklist[src_index].show()
            self.xy_dock_visibility[src_index] = True
            # else:
            #     print name,self.plotManager.dock_dict_list[mode]
    def show_xy(self):
        print('Switching to xy mode')
        cnt = 0
        for single_record in self.datalist_xy:
            for source in single_record:
                if source:
                    for c in source:
                        self.plotManager.addItem(c)
                        cnt += 1
        print(' adding {} curves'.format(cnt))
    def hide_xy(self):
        cnt = 0
        for single_record in self.datalist_xy:
            for source in single_record:
                if source:
                    for c in source:
                        self.plotManager.removeItem(c)
                        cnt += 1
        print('removing xy curves ({})'.format(cnt))
        self.mode_xy = False
    def clear_xy(self):
        self.hide_xy()
        self.datalist_xy = []
    def clear(self):
        self.clear_live_mode()
        self.clear_regular_mode()
        self.clear_xy()
        self.mode = None
    def setup_mode(self,mode):
        self.mode = mode
        if mode == MODE_REGULAR:
            #self.hide_xy()
            #self.clear_live_mode()
            #self.show_regular_mode()
            self.setupplot_regular()
        elif mode == MODE_LIVE:
            self.setupplot_live()
            #self.hide_xy()
            #self.hide_regular_mode()
        elif mode== MODE_XY:
            #self.clear_live_mode()
            #self.hide_regular_mode()
            #self.show_xy()
            self.setupplot_xy()

