#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Daniel
#
# Created:     06.03.2014
# Copyright:   (c) Daniel 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from __future__ import print_function
import re
import time
# 'year','month','day','date' included by default
kwrds = ['year','month','day','date']

def pattern2name(s,d,autoparams= None,nameadd = ''):
    t = time.localtime()
    s = re.sub('%year%',('%d'%t.tm_year),s)
    s = re.sub('%month%',('%02d'%t.tm_mon),s)
    s = re.sub('%day%',('%02d'%t.tm_mday),s)
    s = re.sub('%date%',('%d%02d%02d' % (t.tm_year,t.tm_mon,t.tm_mday)),s)
    if re.findall('%autovalues%',s):
        if autoparams:
            par_s ='_'.join(autoparams.values())
            s = re.sub('%autovalues%',par_s,s)
        else:
            s = re.sub('%autovalues%','',s)
    n = 1
    for key,val in d.items():
            p = '%%%s%%' % key
            if key=='sample': #'sample' substitued by short sample name if ther is one
                if 'SampleShort' in d:
                    ss = d['SampleShort']
                    if ss!='':
                        val = ss

#            if type(val)==unicode:
#                s = re.sub(p,val,s,re.IGNORECASE,re.UNICODE)
#            else:
            s = re.sub(p,str(val),s,re.IGNORECASE)
    if 'SaveNumber' in d: n = d['SaveNumber']

    m = re.search('#+',s)
    while m:
        ms = m.string[m.start():m.end()]
        ns = str(abs(n))
        while len(ns) < len(ms):
            ns = '0'+ns
        s = s[0:m.start()]+ns+s[m.end():]
        m = re.search('#+',s)
    #s = re.sub('__','_',s)
    return s

if __name__ == '__main__':
    teststring = '##%date%_%sample%_spectrum_###'
    d = {'sample':'foo','number':5}
    print(pattern2name(teststring,d))
