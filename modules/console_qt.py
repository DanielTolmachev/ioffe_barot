from __future__ import print_function
__author__ = 'user'
from qtpy import QtCore,QtGui,QtWidgets
import traceback
from console_gui import Ui_ConsoleUi
from outputredirector import output

class QConsole(QtWidgets.QWidget,Ui_ConsoleUi):
    sigStatusChanged = QtCore.Signal(bool)
    def __init__(self,max_lines = 65536):
        super(QConsole,self).__init__()
        self.setupUi(self)
        output.catch()
        output.addOut(self.writeOut)
        output.addErr(self.writeErr)
        self.max_lines = max_lines
        self.textEdit.document().setMaximumBlockCount(max_lines)
    def writeOut(self,s):
        self.textEdit.setTextColor(QtCore.Qt.black)
        # self.textEdit.append(repr(s))
        self.textEdit.moveCursor (QtGui.QTextCursor.End)
        self.textEdit.insertPlainText(s)
        #if self.textEdit.li
    def writeErr(self,s):
        self.textEdit.setTextColor(QtCore.Qt.red)
        self.textEdit.append(s)
    def closeEvent(self, QCloseEvent):
        self.sigStatusChanged.emit(False)
        super(QConsole,self).close()





if __name__ == '__main__':
    import random
    def overflow():
        print(str([random.random() for i in range(0,10000)]))
    app = QtWidgets.QApplication([])
    con = QConsole(max_lines=100)
    con.show()
    print("hello\nhello")
    try:
        1/0
    except:
        traceback.print_exc()
    tmr = QtCore.QTimer()
    tmr.timeout.connect(overflow)
    tmr.start(1)
    app.exec_()