import serial,six,traceback

class SerialA(serial.Serial):
    accepted_kwargs =  ["port",         # number of device, numbering starts at
                                        # zero. if everything fails, the user
                                        # can specify a device string, note
                                        # that this isn't portable anymore
                                        # port will be opened if one is specified
                        "baudrate",     # baud rate
                        "bytesize",     # number of data bits
                        "parity",       # enable parity checking
                        "stopbits",     # number of stop bits
                        "timeout",      # set a timeout value, None to wait forever
                        "xonxoff",      # enable software flow control
                        "rtscts",       # enable RTS/CTS flow control
                        "writeTimeout", # set a timeout for writes
                        "dsrdtr",       # None: use rtscts setting, dsrdtr override if True or False
                        "interCharTimeout"]
    def __init__(self,*args,**kwargs):
        self.is_open = False
        kwargs2 = {k:v for k,v in kwargs.items() if k in self.accepted_kwargs}
        super(SerialA,self).__init__(*args,**kwargs2)
        if hasattr(self,"isOpen"):
            self.is_open = self.isOpen()
        self.connected = self.is_open
        self.address = self.port
    def open(self):
        if not self.is_open:
            try:
                super(SerialA,self).open()
            except:
                traceback.print_exc()
        self.connected = self.is_open
    def write(self,msg):
        if self.is_open:
            try:
                if (type(msg)==str and six.PY3) or (six.PY2 and type(msg)==unicode):
                    msg = msg.encode()
                return super(SerialA,self).write(msg)
            # except serial.portNotOpenError:
            #     pass
            except:
                traceback.print_exc()
    def readline(self,*args):
        if self.is_open:
            try:
                return super(SerialA,self).readline()
            # except serial.portNotOpenError:
            #     return None
            except:
                traceback.print_exc()
    def close(self):
        super(SerialA,self).close()
        self.connected = self.is_open