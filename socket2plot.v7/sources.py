from __future__ import print_function
from numpy import arange
import sys,traceback,json
from os import path,sep,makedirs,listdir,remove
from module_path import PROGRAM_PATH
try:
    import plugins  #for IDE
except:
    print("plugin/ path conatins some errors")



SOURCES_CONFIG_FILE = 'sources.cfg'
SEARCH_SOURCES_JSONS_FOLDER = "~/.lab_python_apps"  #use '~' for hone folder

class SourcesList(list):
    disabled_lst_file = SOURCES_CONFIG_FILE
    json_search_folder = SEARCH_SOURCES_JSONS_FOLDER
    # p = path.realpath(__file__)
    # p = path.dirname(p)
    # p = PROGRAM_PATH

    # print disabled_lst_file
    # def setXvalue(self,src,channel):
    #     for i,chan in enumerate(self[src].channels2show):
    #         if chan== channel:
    #             del self[src].channels2show[i]
    #     self.xvalue = xvalue
    def __init__(self,*args,**kwargs):
        p = PROGRAM_PATH + sep + "config"
        if not path.exists(p):
            makedirs(p)
        self.disabled_lst_file = p + sep + self.disabled_lst_file
        super(SourcesList,self).__init__(*args,**kwargs)
        self.address_dict = {}
        for i,src in enumerate(self):
            self.address_dict[src.address] = i
    def setxrange(self,start,stop,num_of_points):
        self.xvalues = arange(start,stop,(stop-start)/num_of_points)
    def loadstate(self):
        # n = sys.argv[0]  #not correct way, because argv can contain relative paths
        #self.disabled_lst_file = path.split(n)[0]+sep+self.disabled_lst_file
        #print self.disabled_lst_file
        self.loadplugins()
        try:
            f = open(self.disabled_lst_file)
            l = json.load(f)
            if l:
                for d in l:
                    i = self.find(d['name'])
                    # addr = d.get('address','')
                    # cr = d.get('channels2rec',())
                    s = Source(**d)
                    print("loadstate:",s.name,s.xaxis,s.chan_as_x)
                    if i>=0:
                        if self[i].type == "plugin":
                            self[i].update(d)
                        else:
                            self[i] = s
                    else:
                        self.append(s)
##            l = [ln.strip() for ln in f.readlines()]
##            for s in self:
##                if s.name in l:
##                    s.enabled = False
            f.close()
        except:
            #print 'Exception in SourcesList.loadstate\n',sys.exc_info()
            print(traceback.print_exc())
        print('Total amount of signal source ', len(self))
    def loadplugins(self):
        self.instr_plugins = []
        p = PROGRAM_PATH + sep + "plugins"
        sys.path.append(p)
        if path.exists(p) and path.isdir(p):
            files = listdir(p)
            if files:
                import imp
            for f in files:
                bn,ext = path.splitext(f)
                if ext in [".py",".pyw"] and f.startswith("plugin_instrument"):
                    try:
                        mod = imp.load_source(bn, p+sep+f)
                        #from f import __plugintype__,__pluginclass__
                        #print f, "contains plugintype",__plugintype__
                        if hasattr(mod,"__pluginclass__"):
                            classType = getattr(mod,"__pluginclass__")
                            if type(classType)==list:
                                for cn in classType:
                                    self.addPluginSource(cn)
                            else:
                                self.addPluginSource(classType)
                        self.instr_plugins.append(mod)
                        print("module loaded",mod)
                    except ImportError:
                        print(f," is not a plugin")
                        pass
                    except:
                        raise
        # try:
        #     import plugins
        #     plst = dir(plugins)
        #     for pn in plst:
        #         pl = getattr(plugins,pn)
        #         if hasattr(pl,"__plugintype__"):
        #             print pn,"is a plugin of a type", getattr(pl,"__plugintype__")
        #         else:
        #             print pn,"is not a plugin"
        # except ImportError:
        #     print ("plugins import error")
        # except:
        #     traceback.print_exc()
    def addPluginSource(self,pluginclasstype):
        if hasattr(pluginclasstype,"__instrument_info__"):
            instr_info = getattr(pluginclasstype,"__instrument_info__")
            src = Source(**instr_info)
            src.type = "plugin"
            src.pluginclasstype = pluginclasstype
            print("source plugin added",src.name)
            self.append(src)
    def find(self,name):
        # for i,s in enumerate(self):
        #     print i,s.name,name
        for i,s in enumerate(self):
            #print i,s.name,name
            if s.name == name:
                 return i
        return -1
    def savestate(self):
        #f = open("sources2.cfg",'w')
        try:
            f = open(self.disabled_lst_file,'w')
            #f.writelines([s.name+'\n' for s in self if not s.enabled])
            l = [i.__dict__ for i in list_of_sources]
            #print l
            json.dump(l,f,indent = 4,default= lambda x:None)
            ##            for s in self:
            ##                f.write('{ ')
            ##                for key,value in s.__dict__.iteritems():
            ##                    l = '\'{}\':\'{}\',\n'.format(key, value)
            ##                    f.write(l)
            ##                f.write(' }\n')
            f.close()
        except:
            print('Exception in SourcesList.savestate')
            traceback.print_exc()
        print('Total amount of signal source ', len(self))
    def printout(self):
        for src in self:
            print(src.__dict__)
    def loadSourcesJsons(self):
        self.json_search_folder = path.expanduser(self.json_search_folder)
        if path.exists(self.json_search_folder) and path.isdir(self.json_search_folder):
            lst = [f for f in listdir(self.json_search_folder) if f.endswith(".json")]
            s_dict = {}
            for fn in lst:
                fnn = self.json_search_folder+sep+fn
                mtime = path.getmtime(fnn)
                with open(fnn) as f:
                    d = json.load(f)
                    if isinstance(d,dict)\
                        and "Messaging mode" in d\
                        and d["Messaging mode"] == "ZMQ.PUB"\
                        and "address" in d:
                        address = d["address"]
                        # if "*" in address:
                        address = address.replace("*","127.0.0.1")
                        address = address.replace("tcp://","")
                        args = {"address":address}
                        for k,v in d.items():
                            if k not in args:
                                args[k] = v
                        src = Source(**args)
                        if address in s_dict:
                            s1,f1,mt1 = s_dict[address]
                            if mtime>=mt1:
                                s_dict[address] = (src, mtime) #replace old source with new one
                                old = f1
                            else:
                                old = fnn    # ignore this file, because it is older then previously loaded
                            try:
                                remove(old)      # remove old file
                            except:
                                traceback.print_exc()
                        else:
                            s_dict[address] = (src,fnn,mtime)
            if s_dict:
                print("found some data sources (instruments) in", self.json_search_folder)
                for src,_,_ in s_dict.values():
                    if src.address in self.address_dict:
                        idx = self.address_dict[src.address]
                        self[idx] = src
                        print("\t", self[idx].name,"was updated with", src.name,src.address)
                    else:
                        self.append(src)
                        print("\t",src.name,src.address)


class Source():
    def __str__(self):
        return '\'{}\';enabled:{};channels:{}'.format(self.name,
                                    self.enabled,
                                    self.channels2rec)
    def __repr__(self):
        return str(self)
    def __init__(self,address="", channels2rec=[],**kwargs ):
        self.enabled = True
        self.xaxis = None
        self.chan_as_x = None
        self.address = address
        self.type = "zmq_subscriber" # this is a default time cause it is was used initially
        if type(channels2rec) == tuple or type(channels2rec)==list:
            self.channels2rec = channels2rec
        elif type(channels2rec)==int:
            self.channels2rec = (channels2rec,)
        self.timechannel = kwargs.get('timechannel',-1)
        if 'channels2show' in kwargs:
            self.channels2show = kwargs['channels2show']
        else:
            self.channels2show = channels2rec
        if 'channels2params' in kwargs:
            self.channels2params = kwargs['channels2params']
        else:
            self.channels2params = []
        if type(self.channels2show) == int:
            self.channels2show = (self.channels2show,)
        if 'xaxis' in kwargs:
            self.xaxis = kwargs['xaxis']
        if 'chan_as_x' in kwargs:
            self.chan_as_x = kwargs['chan_as_x']
        else:
            self.chan_as_x  = self.xaxis
            # if self.xaxis!=None:
            #     if self.xaxis in self.channels2show:
            #         i = self.channels2show.index(self.xaxis)
            #         self.channels2show = self.channels2show[:i]+self.channels2show[i+1:]
        if 'xlabel' in kwargs:
            self.xlabel = kwargs['xlabel']
        if 'xunits' in kwargs:
            self.xunits = kwargs['xunits']
        if 'name' in kwargs:
            self.name = kwargs['name']
        else: self.name = ''
        if 'description' in kwargs:
            self.description = kwargs['description']
        if 'comment' in kwargs:
            self.comment = kwargs['comment']
        if 'channels_names' in kwargs:
            self.channels_names = kwargs['channels_names']
            if self.timechannel == -1:
                if "time" in self.channels_names:
                    self.timechannel = self.channels_names.index("time")
                elif "Time" in self.channels_names:
                    self.timechannel = self.channels_names.index("Time")

        if 'channels_units' in kwargs:
            self.channels_units = kwargs['channels_units']
        if 'enabled' in kwargs:
            self.enabled = kwargs['enabled']
        self.channels_count = max(len(self.channels_names),len(self.channels2rec),len(self.channels2show))
        if 'subscriber' in kwargs:
            self.subscriber = kwargs['subscriber']
        pass
    def update(self,d):
        for key,val in d.items():
            if key!="pluginclasstype":
                setattr(self,key,val)

#test suite
##list_of_sources = [
##Source('127.0.0.1:5000',3,4,timechannel = 1),
##Source('127.0.0.1:5001',0,timechannel = 1)]

#lock-in + frequency
##list_of_sources = [
##Source('127.0.0.1:5000',1,2,timechannel = 0),
##Source('127.0.0.1:5001',1,timechannel = 0)]

list_of_sources = SourcesList([
#Lock-in.py
# Source(address='127.0.0.1:5000',
#        channels2rec = (1,2),
#         timechannel = 0,
#         name = 'lock-in.py',
#         description = 'Software lock-in detector, written in python',
#         channels_names = ['time','Intensity','deltaI']),

#four stroke light + rf
# Source(address='127.0.0.1:5004',
#        channels2rec = (1,2,3),
#     timechannel = 0,
#     name = '4 stroke lock-in',
#     description = '4 stroke Software lock-in detector',
#     channels_names = ['time','intensity', 'odmr', 'rf_interference', 'dark signal']),

#SR830 Lock-in
Source(address='127.0.0.1:5002',
       channels2rec = (1,2),
    timechannel = 0,
    name = 'SR830',
    description = 'DSP Lock-in amplifier',
    comment = '',
    channels_names = ['time','lockin X','lockin Y'],
    channels_units = ['s','V','V']
),
#Picoscope frequency measure
#Source('127.0.0.1:5001',1,timechannel = 0,xaxis= 1)

#sg384 sweep frequency through lan
# Source(address='127.0.0.1:5003',
#        channels2rec = 1,
#     timechannel = 0,
#     xaxis= 1,
#     xlabel = 'Frequency',
#     xunits = 'Hz',
#     name = 'SG384',
#     comment = 'Signal generator can sweep frequency controlled through lan',
#     channels_names = ['time','frequency']
#     ),

# Source(address='127.0.0.1:5005',
#        channels2rec = 1,
#        timechannel = 0,
#     name = 'PWS4305',
#     comment = 'Proggrammable DC power supply 0-30V,\
#     provide dc magnetic field',
#     xaxis= 1,
#     xlabel = 'Magnet current',
#     xunits = 'A',
#     channels_names = ['time','current']),

# Source(address='127.0.0.1:5006',
#        channels2rec = 1,
#        timechannel = 0,
#     name = 'IPS120',
#     comment = 'Superconducting magnet power supply, IPS series, by Oxford Instruments',
#     xaxis= 1,
#     xlabel = 'Magnetic Field',
#     xunits = 'T',
#     channels_names = ['time','magnetic_field']),

Source( address='127.0.0.1:5008',
	#address='129.217.155.189:5008',
       channels2rec = (1,2),
    timechannel = 0,
    name = 'SR830#2',
    description = 'DSP Lock-in amplifier',
    comment = '',
    channels_names = ['time','lockin X','lockin Y'],
    channels_units = ['s','V','V']),
Source(address='127.0.0.1:5007',
       channels2rec = 1,
    timechannel = 0,
    name = 'Spex',
    description = 'Spex monochromator',
    comment = '',
    xaxis = 1,
    xlabel = 'Wavelength',
    xunits = 'm',
    channels_names = ['time','Wavelength'],
    channels_units = ['s','m'],
    subscriber = "127.0.0.1:6007"),
Source(address='127.0.0.1:5009',
       channels2rec = 1,
       timechannel = 0,
    name = u'Bruker Netzgerat',
    comment = 'power supply for bruker magnet',
    xaxis= 1,
    xlabel = 'Magnetic Field',
    xunits = 'T',
    channels_names = ['time','magnetic_field'],
    channels_units = ['s','T'],
    subscriber = "127.0.0.1:6009"),
Source(address='127.0.0.1:5010',
       channels2rec = 1,
       timechannel = 0,
    name = u'SR830_aux',
    comment = 'aux input of SR830 DSP Lock-in amplifier',
    xaxis= 1,
    xlabel = 'Voltage',
    xunits = 'V',
    channels_names = ['time','aux_in'],
       channels_units=['s', 'V'],),
Source(address='127.0.0.1:5011',
       channels2rec = 1,
    timechannel = 0,
    xaxis= 1,
    xlabel = 'Frequency',
    xunits = 'Hz',
    name = 'DDS3',
    comment = 'DDS generator can sweep frequency controlled through lan',
    channels_names = ['time','frequency'],
    channels_units = ['s','Hz'],
    subscriber = "127.0.0.1:6011"
    ),
Source(address='127.0.0.1:5012',
       channels2rec = 1,
    timechannel = 0,
    #xaxis= 1,
    xlabel = 'Frequency',
    xunits = 'Hz',
    name = 'Oscilloscope_frequency',
    comment = 'An oscilloscope measuring frequency',
    channels_names = ['time','frequency'],
    channels_units = ['s','Hz']
    ),
Source(address='127.0.0.1:5013',
       channels2rec = (1,2),
    timechannel = 0,
    #xaxis= 1,
    xlabel = 'Power',
    xunits = 'dBm',
    name = 'Oscilloscope_power',
    comment = 'An oscilloscope measuring rf power',
    channels_names = ['time','ch1 power',"ch2 power"],
    channels_units = ['s',"dBm","dBm"]
    ),
Source(address='127.0.0.1:5014',
       channels2rec = 1,
    timechannel = 0,
    #xaxis= 1,
    #xlabel = 'Power',
    #xunits = 'dBm',
    name = 'Oscilloscope_voltage',
    comment = 'An oscilloscope measuring various voltage power',
    channels_names = ['time',"voltage"],
    channels_units = ['s',"V"]
    ),
Source(address='127.0.0.1:5015',
       channels2rec = 1,
    timechannel = 0,
    name = 'NIDAQ ai0',
    comment = 'USB NIDAQ devices by National Instruments',
    channels_names = ['time',"voltage"],
    channels_units = ['s',"V"]
    ),
Source(address='127.0.0.1:5016',
       channels2rec = 1,
    timechannel = 0,
    name = 'NIDAQ ai1 (teslameter)',
    comment = 'USB NIDAQ devices by National Instruments',
    channels_names = ['time',"voltage","magnetic field"],
    channels_units = ['s',"V","T"]
    ),
Source(address='127.0.0.1:5017',
       channels2rec = 1,
    timechannel = 0,
    name = 'unnamed',
    comment = 'you can send whatever you want here',
    channels_names = ['time',"X"],
    channels_units = ['s',""],
    xaxis=1,
    xlabel = 'X',
    xunits = ''
    ),
Source(address='127.0.0.1:5018',
       channels2rec = 1,
    timechannel = 0,
    name = 'RF reflectance',
    comment = 'RF reflectance',
    channels_names = ['time',"Reflectance"],
    channels_units = ['s',"dB"]
    )

#obsolete WavescanUSB does not require zmq messenger
# Source(address='127.0.0.1:5017',
#        channels2rec = 1,
#     timechannel = 0,
#     name = 'WavescanUSB',
#     comment = 'Wavescan USB spectrometer',
#     channels_names = ['time',"wavelength"],
#     channels_units = ['s',"nm"]
#     )

])

#list_of_sources = SourcesList() #for experiment purpose only
list_of_sources.loadSourcesJsons()
list_of_sources.loadstate()
#set to True for time beginning at zero
list_of_sources.timerelative = True
#xvalue (source,channel)
list_of_sources.dx = .1
list_of_sources.timeconstant = 0.1
#list_of_sources.setxrange(100.e6,200.e6,1000.)
if __name__ == '__main__':
    # list_of_sources.savestate()
    for src in list_of_sources:
        print(src)

