# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'console_gui.ui'
#
# Created: Wed Sep 21 13:00:20 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from qtpy import QtCore, QtWidgets

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig)

class Ui_ConsoleUi(object):
    def setupUi(self, ConsoleUi):
        ConsoleUi.setObjectName(_fromUtf8("ConsoleUi"))
        ConsoleUi.resize(671, 536)
        self.verticalLayout = QtWidgets.QVBoxLayout(ConsoleUi)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.textEdit = QtWidgets.QTextEdit(ConsoleUi)
        self.textEdit.setReadOnly(True)
        self.textEdit.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByKeyboard|QtCore.Qt.LinksAccessibleByMouse|QtCore.Qt.TextBrowserInteraction|QtCore.Qt.TextSelectableByKeyboard|QtCore.Qt.TextSelectableByMouse)
        self.textEdit.setObjectName(_fromUtf8("textEdit"))
        self.verticalLayout.addWidget(self.textEdit)

        self.retranslateUi(ConsoleUi)
        QtCore.QMetaObject.connectSlotsByName(ConsoleUi)

    def retranslateUi(self, ConsoleUi):
        ConsoleUi.setWindowTitle(_translate("ConsoleUi", "Console", None))

