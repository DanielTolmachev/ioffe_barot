from __future__ import print_function
__author__ = 'Daniel Tolmachev'

import re

def atof(s):
    try:
        f = float(s)
        return f
    except ValueError:
        pass
    if "," in s:
        #try:
            if "." in s:
                    f = float(s.replace(",",""))
            else:
                    f = float(s.replace(",","."))
            return f
        #except ValueError:
            pass
    raise ValueError
if __name__ == '__main__':
    s = "123.15"
    print(s, " = ",atof(s))
    s = "123,15"
    print(s, " = ",atof(s))
    s = "1,230.15"
    print(s, " = ",atof(s))
    s = "1,230.15e7"
    print(s, " = ",atof(s))
    s = "1.230,15"
    print(s, " = ",atof(s))