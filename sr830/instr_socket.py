#-------------------------------------------------------------------------------
# Name:     instr_socket
# Purpose:  base class for devices using socket
#           has open/close/read/write/ask methods
#           it uses create_connection to create a new socket object
#           on each reconnection
#
# Created:     09.04.2015
#-------------------------------------------------------------------------------
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"

import socket,sys,traceback,errno
MAX_REPLY_LENGTH = 100 #bytes
DEFAULT_TIMEOUT = 1 #sec
class Instr_Socket(object):
    max_reply_len = MAX_REPLY_LENGTH
    connected = False
    socket = socket.socket()
    def __init__(self,address):
        #super(Instr_Socket,self).__init__(socket.AF_INET, socket.SOCK_STREAM)
        #self.settimeout(DEFAULT_TIMEOUT)
        if type(address)==tuple:
            if len(address)>1:
                self.address = address[0]
                self.port = address[1]
        elif type(address)==str:
##            port_i = address.rfind(':')
##            add_i = address.rfind('/')
##            if add_i<0:
##                add_i = 0
##            self.port = int(address[a:])
##            self.port
            pass
    def write(self,data):
        try:
           # if self.connected:
               if type(data)==str:
                   self.socket.send(data.encode("utf-8"))
               else:
                   self.socket.send(data)
        except socket.error as err:
                if err.errno == errno.WSAECONNRESET:
                    self.connected = False
                else:
                    print("unhandled socket error",err.errno,err)
                    raise err
        except:
            #print "Error while sending",data
            traceback.print_exc()


    def read(self):
        #try:
            #if self.connected:
                return self.socket.recv(self.max_reply_len)
        #except socket.error as err:
        #         if err.errno == errno.WSAECONNRESET:
        #             self.connected = False
        #         else:
        #             print "unhandled socket error",err
        # except:
        #     traceback.print_exc()
        #     return ''
    def open(self,*address):
        try:
            if self.connected==True:
                print("already connected")
            if len(address)>0:
                self.address = address[0]
            if type(self.address)==tuple:
                print(self.address,"->",self.address[0])
                self.address = self.address[0]
            self.socket = socket.create_connection((self.address,self.port),DEFAULT_TIMEOUT)
            self.connected = True
        except:
            traceback.print_exc()
            print((self.address,self.port))
    def create_connection(self):
        if type(self.address)==tuple:
            print(self.address,"->",self.address[0])
            self.address = self.address[0]
        self.socket = socket.create_connection((self.address,self.port),DEFAULT_TIMEOUT)
        self.connected = True
    def ask(self,data):
        self.write(data)
        return self.read()





