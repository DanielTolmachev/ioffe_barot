#-------------------------------------------------------------------------------
# Name:        sweeper with additional features, like:
#       waiting some time for value to settle at beginning of scan
#       waiting some time when passing through special glitch points
#               (helps outwait moments, when equipments is
#                    switching between modes etc.)
# Author:      Daniel.Tolmachev@gmail.com
#
# Created:     13.03.2015
# Copyright:   (c) Daniel 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import time
from sweeper import Sweeper
class SweeperWGlitch(Sweeper):
    wait_at_begin = 0
    glitch_points = []
    times_to_wait = 3 #time to wait in timeconstants (dt)
    def __init__(self,**kwargs):
        """
            wait_at_begin = False
            glitch_points = [] list of glitch points

        """
        if kwargs.has_key('wait_at_begin'):
            self.wait_at_begin = kwargs.pop('wait_at_begin')
        if kwargs.has_key('glitch_points'):
            gp = kwargs.pop('glitch_points')
            if type(gp)==list:
             self.glitch_points = gp
        if kwargs.has_key('times_to_wait'):
            self.times_to_wait = kwargs.pop('times_to_wait')
        super(SweeperWGlitch,self).__init__(**kwargs)
    def sweep(self,start,stop,step,dt,resume = False,
                loop =  True,
                bidirectional = False,
                maxloops = 0):
        self.stopped = False
        self.maxloops = maxloops
        self.loops = 0
        self.resume = resume
        self.loop = loop
        self.bidirectional = bidirectional
        self.reverse = False
        if start>stop:
            start,stop = stop,start
        if start==stop: #no sweep, just set value
            self.setfunc(start)
            return
        if self.resume:
            self.value = self.queryfunc()
        else:
            if step>0:
                self.value = start
            else:
                self.value = stop
        print 'Sweeping from {} to {}'.format(start,stop)
        if self.wait_at_begin: #wait if needed
                print 'Waiting {} seconds'.format(dt*self.times_to_wait)
                time.sleep(dt*self.times_to_wait)
        glitch = False
        while not self.stopped:
            self.setfunc(self.value)
            if glitch:
                glitch = False
                print 'Waiting {} seconds'.format(dt*self.times_to_wait)
                time.sleep(dt*self.times_to_wait)
                self.glitch_notify('no skip')
            if self.verbose:
                #print self.value
                print self.queryfunc()
            prev = self.value
            self.value +=step
            if self.value>stop or self.value<start:
            #stop
                if not self.loop:
                    if not self.bidirectional or \
                        self.bidirectional and self.reverse: # two way but backward sweep
                            self.value-=step #stop at end not after end
                            break #stop sweeping
            #continue
                #bidirectional
                if self.bidirectional:
                    self.reverse = not self.reverse
                    step = step*-1
                    if self.value>=stop: self.value = stop
                    elif self.value<=start: self.value = start
                #unidirectional
                else:
                    if self.value>=stop: self.value = start
                    elif self.value<=start: self.value = stop
                if not self.reverse:
                    self.loops+=1
                if self.maxloops:
                    if self.loops>maxloops: break
            if self.glitch_points:
                for pt in self.glitch_points:
                    if self.value<pt<prev or self.value>pt>prev:
                        glitch = True
                        self.glitch_notify('skip')
            time.sleep(dt)
        print 'Sweep has stopped'
    def glitch_notify(self,status):
            print 'glitch {}  {}'.format(status,self.value)


if __name__ == '__main__':
    print 'test various regimes'
    sw = SweeperWGlitch(glitch_points = [65.2],wait_at_begin = True )
    print 'up once'
    sw.sweep(0,100,10,.01,loop = False)
    print 'down once'
    sw.sweep(30,100,-1,.01,loop = False)
    print 'resume up and down once'
    sw.sweep(0,100,1,.01,loop = False,bidirectional = True,resume = True)
    print 'loop'
    sw.sweep(0,10,1,.01,maxloops = 3)
