#-------------------------------------------------------------------------------
#
# class UsbtmcExc is wrapper for usbtmc.Instrument class
# with exception handling
#
# Author:      Daniel Tolmachev
#
# Created:     17.03.2015
# Copyright:   (c) Daniel 2015
#-------------------------------------------------------------------------------
import usbtmc
import sys
class UsbtmcExc(usbtmc.Instrument):
    lastError = None
    connected = False
    def __init__(self,vid,pid):
        self.vid = vid
        self.pid = pid
        self.connect()
    def connect(self):
        try:
            super(UsbtmcExc,self).__init__(self.vid,self.pid)
            self.connected = True
##        self.idn = self.ask('*IDN?\n')
##        print idn
        except:
            print 'cannot connect to USBTMC device {}:{}'.format(self.vid,self.pid)
            print sys.exc_info()
    def ask(self,msg):
        try:
            return usbtmc.Instrument.ask(self,msg)
        except usbtmc.usbtmc.usb.core.USBError,e:
            if e.backend_error_code==-116:
                msg = 'request to usbtmc timed out'
                print msg
                return msg
            self.process_exception(e)
        except:
            e = sys.exc_info()
            self.process_exception(e)
        return ''
    def process_exception(self,e):
            print 'Error while asking usbtmc.Instrument'
            self.lastError = e
            print sys.exc_info()
            print self.lastError