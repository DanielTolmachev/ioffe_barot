from qtpy import QtWidgets,QtCore,QtGui
import os,re,subprocess,socket,threading,traceback
# from multiprocessing.pool import ThreadPool
import serial
from serial.tools import list_ports

def list_serial_ports():
    # Windows
    if os.name == 'nt':
        # Scan for available ports.
        available = []
        for i in range(256):
            try:
                s = serial.Serial('COM'+str(i + 1))
                available.append('COM'+str(i + 1))
                s.close()
            except serial.SerialException:
                pass
        return available
    else:
        # Mac / Linux
        return [port[0] for port in list_ports.comports()]

class DevAddrWBase(QtWidgets.QWidget):
    newAddressSet = QtCore.Signal(object)
    connectionRequested = QtCore.Signal(bool)
    def __init__(self, current_address = None):
        if callable(current_address):
            self.current_address = current_address()
        else:
            self.current_address = current_address
        super(DevAddrWBase,self).__init__()
        self.worker = Worker()
        self.w_thrd = QtCore.QThread()
        self.worker.moveToThread(self.w_thrd)
        self.w_thrd.start()
    def __del__(self):
        # print("closing threads")
        self.w_thrd.quit()
        self.w_thrd.wait(100)

class SerialSelectorW(DevAddrWBase):
    def __init__(self):
        super(SerialSelectorW,self).__init__()
        lo = QtWidgets.QGridLayout()
        self.rblo = QtWidgets.QVBoxLayout()
        self.setLayout(lo)
        self.lbl_cur = lo.addWidget(QtWidgets.QLabel("current port {}".format(self.current_address)))
        lo.addWidget(QtWidgets.QLabel("list of available ports"))
        self.btn_refresh = QtWidgets.QPushButton("refresh")
        self.btn_refresh.pressed.connect(self.updatePortList)
        lo.addWidget(self.btn_refresh,lo.rowCount()-1,1)
        lo.addLayout(self.rblo,lo.rowCount(),0,1,2)
        self.rbl = []
        lo.addItem(QtWidgets.QSpacerItem(0,0,QtWidgets.QSizePolicy.Expanding,QtWidgets.QSizePolicy.Minimum),0,3)
        lo.addItem(QtWidgets.QSpacerItem(0,0,QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Expanding),lo.rowCount(),0)
        self.updatePortList()
    def updatePortList(self):
        serports = ["None"]+list_serial_ports()
        while self.rbl:
            rb = self.rbl.pop()
            # rb = self.rblo.takeAt(0)
            self.rblo.removeWidget(rb)
            # rb.destroy()
            rb.setParent(None)
            del rb
        for sp in serports:
            rb = QtWidgets.QRadioButton(sp)
            self.rblo.addWidget(rb)
            self.rbl.append(rb)
            rb.toggled.connect(self.onRBcheck)
    def onRBcheck(self,state):
        if state:
            sndr = self.sender()
            if sndr in self.rbl:
                self.newAddressSet.emit(sndr.text())
                print("Serial port was chosen:",sndr.text())

class IPselectorW(DevAddrWBase):
    def __init__(self,default_ip_port = None):
        super(IPselectorW,self).__init__()
        self.port = default_ip_port
        lo = QtWidgets.QGridLayout()
        self.setLayout(lo)
        self.lbl_cur = lo.addWidget(QtWidgets.QLabel("current address:"))
        self.cb = QtWidgets.QComboBox()
        f = self.cb.font()
        fm = QtGui.QFontMetrics(f)
        self.cb.setMinimumWidth(fm.width("255.255.255.255.255"))
        self.cb.setEditable(True)
        self.cb.setEditText("{}".format(self.current_address))
        lo.addWidget(self.cb,0,1)
        self.le_port = QtWidgets.QLineEdit()
        self.le_port.setText(str(self.port))
        w = fm.width("655355")
        # self.le_port.setMinimumWidth(w)
        self.le_port.setFixedWidth(w)
        lo.addWidget(self.le_port, 0, 2)
        # lo.addWidget(QtWidgets.QLabel("list of lan ip's"))
        self.btn_refresh = QtWidgets.QPushButton("list of lan ip's (rescan)")
        self.btn_refresh.pressed.connect(self.scanLAN)
        lo.addWidget(self.btn_refresh,lo.rowCount(),0,1,3)
        self.listw = QtWidgets.QTableWidget()
        self.listw.setShowGrid(False)
        self.listw.verticalHeader().hide()
        self.listw.verticalHeader().setDefaultSectionSize(self.listw.verticalHeader().minimumSectionSize())
        self.listw.horizontalHeader().hide()
        self.listw.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.listw.itemDoubleClicked.connect(self.itemDoubleClicked)
        lo.addWidget(self.listw, lo.rowCount(),0,1,3)
        self.worker.scan_complete.connect(self.on_scan_complete)
        self.worker.port_status_checked.connect(self.on_port_status_checked)
        # self.scanLAN()
    def scanLAN(self):
        try:
            self.port1 = int(self.le_port.text())
            if not (0 <self.port1 <65536):
                raise("port should be in range 0-65535")
        except:
            self.port1 = self.port
        QtCore.QMetaObject.invokeMethod(self.worker, "scanLAN", QtCore.Qt.AutoConnection, QtCore.Q_ARG(object, self.port1))
    def updateListView(self):
        self.listw.clear()
        self.listw.setRowCount(len(self.ips))
        self.listw.setColumnCount(1)
        for i,ip in enumerate(self.ips):
            it = QtWidgets.QTableWidgetItem(ip)
            it.setFlags(it.flags()^QtCore.Qt.ItemIsEditable)
            self.listw.setItem(i,0,it)
            self.listw.resizeRowToContents(self.listw.currentRow())
    def on_scan_complete(self,lst):
        self.ips = lst
        self.updateListView()
    # def updateListView2(self):
    #     for ip,status in zip(self.ips,self.ips2):
    #         if status:
    #             self.listw.addItem("{} Ready".format(ip))
    #         else:
    #             self.listw.addItem(ip)
    def on_port_status_checked(self,args):
        i,status = args
        if status:
            self.listw.setColumnCount(2)
            it = QtWidgets.QTableWidgetItem()
            it.setFlags(it.flags() ^ QtCore.Qt.ItemIsEditable)
            it.setText(str(self.port1))
            # it.setTextColor(QtGui.QColor("green"))
            it.setForeground(QtGui.QBrush(QtGui.QColor("green")))
            self.listw.setItem(i,1,it)
        else:
            it = self.listw.item(i,0)
            #print(args, it.text())
            # it.setTextColor(QtGui.QColor("gray"))
            it.setForeground(QtGui.QBrush(QtGui.QColor("gray")))
    def itemDoubleClicked(self,it):
        ip = self.listw.item(it.row(), 0).text()
        self.cb.setEditText(ip)
        self.newAddressSet.emit((ip,self.port))


class GPIBparamW(QtWidgets.QFrame):
    GPIB_SELECTION_STR_HELP = """command to select device with gpib address

for example, Prologix GPIB-LAN, or GPIB-USB adaptors use
"++addr 7"  command to address device with GPIB address 7"""
    def __init__(self, current_gpib_address = None):
        super(GPIBparamW,self).__init__()
        lo = QtWidgets.QGridLayout()
        self.setLayout(lo)
        lo.addWidget(QtWidgets.QLabel("GPIB addrress:"))
        self.cb = QtWidgets.QSpinBox()
        self.cb.setMinimum(1)
        self.cb.setMaximum(31)
        lo.addWidget(self.cb,0,1)
        if current_gpib_address:
            self.cb.setValue(current_gpib_address)
        lo.addWidget(QtWidgets.QLabel("GPIB device selection string"),lo.rowCount(),0,1,2)
        self.cba = QtWidgets.QComboBox()
        self.cba.setEditable(True)
        self.cba.setToolTip(self.GPIB_SELECTION_STR_HELP)
        self.cba.addItem("++addr ")
        lo.addWidget(self.cba,lo.rowCount(),0,1,2)

# class CheckPort(QtCore.QThread):
#     def __init__(self, i, ip, port):
#         self.idx = i
#         self.ip = ip
#         self.port = port
#         super(CheckPort,self).__init__()
#         self.port_status = False
#     def run(self):
#         try:
#             #print("trying", self.idx, self.ip, self.port, end="")
#             soc = socket.create_connection((self.ip, self.port), timeout=1)
#             # self.ips2.append(True)
#             self.port_status = True
#     #         print(" OK")
#             soc.close()
#     #         # self.port_status_checked.emit((i, True))
#         except:
#             #print(" closed")
#             # self.port_status_checked.emit((i, False))
#     #         # self.ips2.append(False)
#             self.port_status = False

class Worker(QtCore.QObject):
    scan_complete = QtCore.Signal(object)
    port_status_checked = QtCore.Signal(object)
    re_ip = re.compile("(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})")
    def __init__(self):
        super(Worker,self).__init__()
    @QtCore.Slot(object)
    def scanLAN(self,port):
        self.ips = []
        response = subprocess.check_output("arp -a")
        if response:
            response = response.splitlines()
            for l in response:
                m = re.search(self.re_ip, l.decode())
                if m:
                    self.ips.append(m.group())
        self.scan_complete.emit(self.ips)
        if port:
            #self.try_ports_simple(port)
            self.try_ports(port)
            pass
    def try_ports_simple(self,port):
        try:
            self.ips2 = []
            for i,ip in enumerate(self.ips):
                self.checkPort(i,ip,port)
        except:
            traceback.print_exc()
    def try_ports(self,port):
        self.ips2 = []
        for i,ip in enumerate(self.ips):
            t = threading.Thread(target=self.checkPort,args = (i,ip,port))
            t.start()

    def checkPort(self, i, ip, port):
        try:
            # print("trying",i, ip, port,end="")
            soc = socket.create_connection((ip, port), timeout=1)
            self.ips2.append(True)
            soc.close()
            # print(" OK")
            self.port_status_checked.emit((i, True))
            return True
        except:
            # print(" closed")
            self.port_status_checked.emit((i, False))
            self.ips2.append(False)
            return False

GPIBoverIP = 0
GPIBoverSerial = 1

class GPIBselectorW(DevAddrWBase):
    def __init__(self,mode = GPIBoverSerial):
        super(GPIBselectorW,self).__init__()
        lo = QtWidgets.QVBoxLayout()
        self.setLayout(lo)
        if mode == GPIBoverSerial:
            self.addrW = SerialSelectorW()
        elif mode == GPIBoverIP:
            self.addrW = IPselectorW()
        lo.addWidget(self.addrW)
        self.gpibW = GPIBparamW()
        lo.addWidget(self.gpibW)

class DeviceAddressWidget(QtWidgets.QWidget):
    sigNewAddressSet = QtCore.Signal(object)
    sigConnectionRequested = QtCore.Signal(bool)
    sigConnected = QtCore.Signal(bool)
    sigConnectionStatus = QtCore.Signal(object)
    SYNONYMS_SERIAL = ["serial","rs232","rs-232"]
    SYNONYMS_IP = ["ip", "tcp", "lan", "ethernet"]
    def __init__(self, current_address = None, interfaces = [], default_ip_port = None,device_object = None,openfunc = None,
                                                   closefunc = None, func_is_open = None):
        """
        :param current_address: currently used address
        :param interfaces: supported interfaces can be "serial" ("rs232","rs-232"),
                                                       "ip" ("ethernet","lan","tcp"),
                                                       "gpib over serial"
                                                       "gpib over ip" ("gpib over ethernet")
            device_object: object with open/close/isOpen methods
            openfunc, closefunc,func_is_open: if you provide function for opening(connecting)/closing(disconnecting) device
             or object with open/close/isOpen, you can then use open/close slots for opening/closing
             device qt style (with emitting signals)
        """
        self.interfaces = [i.lower() for i in interfaces]
        self.address = current_address
        self.new_address = None
        #open/close/isOpen functions
        self._openfunc = self._closefunc = self._func_is_open = None
        if device_object:
            if hasattr(device_object,"open"):
                self._openfunc = device_object.open
            if hasattr(device_object, "close"):
                self._closefunc = device_object.close
            if hasattr(device_object, "isOpen"):
                self._func_is_open = device_object.isOpen
        if openfunc:
            self._openfunc = openfunc
        if closefunc:
            self._closefunc = closefunc
        if func_is_open:
            self._func_is_open = func_is_open

        super(DeviceAddressWidget,self).__init__()
        gl = QtWidgets.QGridLayout()
        self.setLayout(gl)
        tw = QtWidgets.QTabWidget()
        gl.addWidget(tw)
        self.bb = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Apply|
                                        QtWidgets.QDialogButtonBox.Ok|
                                        QtWidgets.QDialogButtonBox.Cancel)
        # self.btn_apply = QtWidgets.QPushButton()
        # bb.addButton()
        gl.addWidget(self.bb)
        self.bb.accepted.connect(self.on_accepted)
        self.bb.rejected.connect(self.close)
        self.bb.button(self.bb.Apply).pressed.connect(self.apply)
        for i in self.interfaces:
            w = None
            if i in self.SYNONYMS_SERIAL:
                w = SerialSelectorW()
                tw.addTab(w,"Serial")
            if i in self.SYNONYMS_IP:
                w = IPselectorW(default_ip_port = default_ip_port)
                tw.addTab(w,"ip")
            if "gpib" in i:
                if any([s in i for s in self.SYNONYMS_SERIAL]):
                    w = GPIBselectorW(GPIBoverSerial)
                    tw.addTab(w, "GPIB over Serial")
                elif any([s in i for s in self.SYNONYMS_IP]):
                    w = GPIBselectorW(GPIBoverIP)
                    tw.addTab(w, "GPIB over IP")
            if w:
                w.newAddressSet.connect(self.onNewAddressSet)
                w.connectionRequested.connect(self.sigConnectionRequested)

        tw.adjustSize()
        self.adjustSize()
        self._emitStatuses()
    def apply(self):
        self.address = self.new_address
        self.sigNewAddressSet.emit(self.new_address)
    def on_accepted(self):
        self.apply()
        self.close()
    def onNewAddressSet(self,address):
        self.new_address = address
        if self.new_address!=self.address:
            self.bb.button(QtWidgets.QDialogButtonBox.Apply).setEnabled(True)
        else:
            self.bb.button(QtWidgets.QDialogButtonBox.Apply).setEnabled(False)
    def open(self):
        if self._openfunc:
            self._openfunc()
        else:
            print('''DeviceAddressWidget: no open() function set,
use "openfunc" keyword to provide function for opening (connecting to) device''')
        self._emitStatuses()
    def close(self):
        if self._closefunc:
            self._closefunc()
        else:
            print('''DeviceAddressWidget: no close() function set,
use "closefunc" keyword to provide function for closing (disconnecting from) device''')
        self._emitStatuses()
    def _emitStatuses(self):
        if self._func_is_open:
            if self._func_is_open():
                self.sigConnected.emit(True)
                self.sigConnectionStatus.emit("connected to {}".format(self.address))
            else:
                self.sigConnected.emit(False)
                self.sigConnectionStatus.emit('disconnected, "{}"'.format(self.address))



if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    interfaces = ["serial","ethernet","gpib over ethernet","gpib over serial"]
    w = DeviceAddressWidget(None,interfaces,default_ip_port=1234)
    w.show()
    app.exec_()