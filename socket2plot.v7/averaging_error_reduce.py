from __future__ import print_function
import numpy as np
import pyqtgraph as pg
import sys

def _round_to_float(x,err):
    if err==0:
        return x
    else:
        return round(x,-int(np.round(np.log10(err))))

XCHANNEL = (1,2)
YCHANNEL = (0,3)
MINACCUMPOINTS = 100
XSTEP = 1e6 #1MHz resolution

class _data():
    xlist = list()
    data = list()
    average = list()
    error = list()
    xprev = 0
    xcnt = 0
    index = None
    gw = None
    errb =None
    def __init__(self,step=XSTEP):
        self.step = step
##    def addxy(x,y):
##        xr = _round_to_err(x,step)
##        if xr in xlist:
##            i = xlist.find(xr)
##        else:
    def add(self,src,chan,data):
        #print (src,chan,data)
        if (src,chan) == XCHANNEL:
            xr = _round_to_float(data,self.step)
            if xr in self.xlist:
                self.index = self.xlist.index(xr)
            else:
                self.index = None
                if self.xcnt<MINACCUMPOINTS:
                    if self.xprev == xr:
                        self.xcnt+=1
                    else:
                        self.xprev = xr
                    #print xr
                else:
                    self.xlist.append(xr)
                    self.data.append([])
                    self.average.append(None)
                    self.error.append(None)
                    self.index = len(self.xlist)-1
                    print('%f appended to list' %xr)
                    self.xcnt = 0
        if (src,chan) == YCHANNEL:
            self.accumulate(data)
            self.plot()
    def accumulate(self,y):
        if self.index!=None:
            if self.data[self.index]==[]:
                self.data[self.index] =(y,0,1,0)
                self.average[self.index] = y
                self.error[self.index] = y
            else:
                av,diff,cnt,prev = self.data[self.index]
                av +=y
                if cnt>1:
                    diff+=(y-prev)**2
                prev = y
                cnt +=1
                err = np.sqrt(diff)/cnt
                #print y,av/cnt,err
                self.data[self.index] = (av,diff,cnt,prev)
                self.average[self.index] = av/cnt
                self.error[self.index] = err
    def plot(self):
        try:
            if self.gw:
    ##            self.errb = pg.ErrorBarItem(x = self.xlist,y = self.average,
    ##            height = self.error)
    ##            self.gw.addItem(self.errb)
                self.gw.clear()
                x = np.array(self.xlist)
                y = np.array(self.average)
                err = np.array(self.error)
                self.errb = pg.ErrorBarItem(x = x,y = y,height = err,beam=XSTEP)
                self.gw.addItem(self.errb)
                self.gw.plot(self.xlist,self.average,symbol='o', pen=None)
            else:
                self.gw = pg.plot()
        except:
            print(sys.exc_info())
##            self.errb = pg.ErrorBarItem()
##            self.gw.addItem(self.errb)




data = _data()

##if __name__ == '__main__':
##    a = np.random.normal(size = 1000)
##    data = _data(1)
##    for i,f in enumerate(a):
##        data.addxy(f)