#-------------------------------------------------------------------------------
# Name:        modes conversion
# Purpose:
#
# Author:      Daniel Tolmachev
#
# Created:     18.03.2015
# Copyright:   (c) Daniel 2015
#-------------------------------------------------------------------------------
base_units = ['A']
SI_miltipliers = [
('G',1.e9)
('M',1.e6),
('k',1.e3),
('m',1.e-3),
('u',1.e-6),
('n',1.e-9)
]
units_table = [
()
]
class UnitConverter(object):
    def __init__():
        self.SI_prefixes = [t[0] for t in SI_miltipliers]
    def conv2SI(val,units):
        if units[0] in self.SI_prefixes:
            i = self.SI_prefixes.index(units[0])
            return val/SI_miltipliers[i][1]
    def convfromSi(val,units):
        if units[0] in self.SI_prefixes:
            i = self.SI_prefixes.index(units[0])
            return val*SI_miltipliers[i][1]
uconv = UnitConverter()
if __name__ == '__main__':
    print uconv.conv2SI(30,'mA')
    print uconv.convfromSi(2.8e6,'GHz')