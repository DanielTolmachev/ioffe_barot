import sys,logging,traceback,time
from math import nan
sys.path.append( '../modules')
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev"

#MAXRATE = .4 / 60.  #Tesla/seconds
MAXRATE = .6 / 60.  #Tesla/seconds
MAXFIELD = 7 #Tesla
FIELDS = [4,5,6]   # fields when we need to lower rate
RATES = [.3/60., .25/60., .15/60.] #maximal allowed rate for fields


LAN = ['socket','ip','lan','ethernet']
COM = ['com','serial','rs232','rs-232']
INTERFACES = [LAN,COM]


class magnet_IPS(object):
    proxy = None
    opened = False
    idn = ''
    def __init__(self,interface_type,dev_address):
        self.status = {}
        self.remote = 0
        self._read = self._write = self._open = self.close = self.dummy
        self.__init__interface__(interface_type,dev_address)
    def __init__interface__(self,interface_type,dev_address):
        self.dev_address = dev_address
        self.interface_type = interface_type
        if type(interface_type)==str:
            if interface_type.lower() in LAN:
                from instr_socket import Instr_Socket
                interface_type = Instr_Socket
            elif interface_type.lower() in COM:
                import serial
                self.interface = serial.Serial()
                self.interface.port = dev_address
                self.interface.timeout = 1
                self._write = self.interface.write
                self._read = self.interface.readline
                self.close = self.interface.close
                self._open = self.interface.open
                self.setRemote()
                self.setLF()
                #self.setSlow()
            elif interface_type.lower() == "dummy":
                self.dev_address = "dummy"
                self.write = self.dummy
                self.read = self.dummy
                self.close = self.dummy
                self.open = self.dummy
                self.field = 0
                self.setField = self._dummySetField
                self.queryField = self._dummyQueryField
                self.go = self._dummySetTargetField
            else:
                self._wrongInterface_msg()
                return
        else:
            self._wrongInterface_msg()
            return
        if hasattr(interface_type,'read') and \
                hasattr(interface_type,'write') and\
                hasattr(interface_type,'open'):
            if not self.proxy:
                self.proxy = interface_type(dev_address)
                self._read = self.proxy.read
                self._write = self.proxy.write
                self.open = self.proxy.open
                self.close = self.proxy.close
        #self.open(dev_address)
    def _wrongInterface_msg(self):
        print('magnet class support these types of interfaces')
        for i in INTERFACES:
            print(i)
    def setAddress(self,dev_address):
        self.__init__interface__(self.interface_type,dev_address)
    def setLF(self):
        return self.ask(b'Q2\r')
    def dummy(self,*args):
        print('empty function called with',args)
    def ask(self,query):
        self.write(query)
        return self.read()
    def read(self):
        if self.interface.isOpen():
            return self._read()
    def write(self,msg):
        if self.interface.isOpen():
            if not self.remote:
                self._setRemote()
            return self._write(msg)
    def open(self):
        self._open()
    def getID(self):
        if not self.idn:
            self.idn = self.ask(b'V\r')
        return self.idn    
    def setRemote(self):
        rep = self.ask(b'C3\r')
        print("setting device to remote mode c3 ->",rep)
        return rep # C3 = Remote & Unlocked
    def _setRemote(self):
        self._write(b'C3\r')
        rep = self.read()
        print("setting device to remote mode c3 ->",rep)
        return rep # C3 = Remote & Unlocked
    def setSlow(self):
        return self.ask(b'M5\r') # M5  Tesla, Slow
    def setField(self,field):
        if field>MAXFIELD:
            field = MAXFIELD
        self.setFieldTarget(field)
        rate = MAXRATE
        for i in reversed(range(0, len(FIELDS))):
            if field > FIELDS[i]:
                rate = RATES[i]
                print ('rate reduced to {} T/min'.format(rate*60))
                break
        self.setFieldRate(MAXRATE)
        self.go()
    def queryStatus(self):
        rep = self.ask(b"X\r")
        if rep and rep.startswith(b"X"):
            try:
                self.status["string"] = rep
                i = rep.index(b"H")+1
                v = int(rep[i:i+1])
                self.status["heater"] = v
                i = rep.index(b"A")+1
                v = int(rep[i:i+1])
                self.status["A"] = v
                i = rep.index(b"C")+1
                v = int(rep[i:i+1])
                self.status["remote"] = v
                self.remote = True if v in [3,4] else False
            except:
                traceback.print_exc()
        return self.status
    def _dummySetField(self, field):
        self.field = field
    def _dummyQueryField(self):
        return self.field
    def _dummySetTargetField(self):
        self.field = self.field_target
    def go(self):
        return self.ask(b'A1\r')
    def stop(self):
        return self.ask(b'A0\r')
    def gotoZero(self):
        return self.ask(b'A2\r')
    def queryField(self):
        return self.queryValue(7)
    def queryFieldPersistent(self):        
        return self.queryValue(18)
    def queryValue(self,R_number):
        ret = self.ask(b'R%d\r' % R_number)
        while ret:
            if ret[0:1] == b'R':
                res = float(ret[1:].strip())
                # if res>10:
                #     print ret
                return res
            else:
                logging.debug("queryValue 'R{}' returned incorrect answer: {}".format(
                        R_number,ret))
                ret = self.read()
        return nan
    def queryFieldRate(self):
        ret = self.ask(b'R9\r')
        if ret[0:1] == b'R':
            return float(ret[2:].strip())/60 #convert T/min to T/s
    def queryFieldTarget(self):
        ret = self.ask(b'R8\r')
        if ret and ret[0:1] == b'R':
            return float(ret[2:].strip())
    def stat(self):
        return self.ask(b'X\r')
    def setFieldTarget(self,field):
        self.field_target = field
        ret = self.ask('J{}\r'.format(field).encode())
        return ret
    def setFieldRate(self,rate):
#        print("set rate is disabled"); return;
        if rate>MAXRATE:
            rate = MAXRATE
        ret = self.ask('T{}\r'.format(rate*60).encode()) #convert T/s to T/min
        return ret
    def setHeater(self,state):
        if state:
            ret = self.ask(b"H1\r")            
        else:
            ret = self.ask(b"H0\r")
        print("setting heater",state,ret)
        time.sleep(2)
    def getHeater(self):
        return self.queryValue(5) # R5 for heater
if __name__ == '__main__':
    mg = magnet_IPS('serial','COM2')
    print( mg.getID())
