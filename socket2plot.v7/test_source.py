from __future__ import print_function
import zmq
from random import choice
import numpy as np
import time,sys
from test_source_gui import Ui_MainWindow
from qtpy import QtCore,QtWidgets
sys.path.append( '../modules')
from options import Options
from QMainWndSavGeom import QMainWindowAutoSaveGeometry


context = zmq.Context()
ss = context.socket(zmq.PUB)  #signal socket
ss_port = 5008
ss.bind("tcp://127.0.0.1:{}".format(ss_port))
ss2 = context.socket(zmq.PUB)  #signal socket
ss2_port = 5002
ss2.bind("tcp://127.0.0.1:{}".format(ss2_port))
sf = context.socket(zmq.PUB)  #frequency socket
sf_port = 5011
sf.bind("tcp://127.0.0.1:{}".format(sf_port))
swl = context.socket(zmq.PUB)  #wavelength socket
swl_port = 5007
# swl.bind("tcp://127.0.0.1:{}".format(swl_port))
sfld = context.socket(zmq.PUB)  #field socket
sfld_port = 5009
# sfld.bind("tcp://127.0.0.1:{}".format(sfld_port))

i = 0
period = 30  #sec
totaltime = 100000
dt = 0.1
f0 = 2840.
f1 = 2940.
dtf = 0.2  #s per frequency step
dts = 0.1
dts2 = 0.3
df = (f1 - f0) / period * dtf
speed = (f1 - f0) / period
start = time.time()
cycles = totaltime / dt
f2 = f0
sig_cycles = 0
sig2_cycles = 0
def simNVfreq(x):
    return np.exp(-(x - 2875) ** 2 / 20e3) + np.exp(-(x - 2885) ** 2 / 20)
def simField(x):
    return np.sin(x)
def simWl(x):
    return np.exp(-(x - 810e-9) ** 2 / (2e-9)**2) + np.exp(-(x - 840e-9) ** 2 / 15e-9**2)+\
np.exp(-(x - 870e-9) ** 2 / 20e-9**2)*.8

class MainWnd(Ui_MainWindow,QMainWindowAutoSaveGeometry):
    dt = 100 #ms
    timer_freq = QtCore.QTimer()
    timer_field = QtCore.QTimer()
    timer_wl = QtCore.QTimer()
    period = 30  #sec
    dtf = 0.2  #s per frequency step
    dts = 0.1
    dts2 = 0.3
    MODE_FREQUENCY = 1
    MODE_FIELD = 2
    MODE_WAVELENGTH = 3
    releaseAfterUse = False
    def __init__(self):
        super(MainWnd,self).__init__()
        self.setupUi(self)
        self.timer_freq.timeout.connect(self.freq)
        self.timer_field.timeout.connect(self.field)
        self.timer_wl.timeout.connect(self.wl)
        if not self.releaseAfterUse:
            sfld.bind("tcp://127.0.0.1:{}".format(sfld_port))
    def on_rb_freq_toggled(self,state):
        if state:
            self.stop()
            print("freq")
            if self.checkBox.isChecked():
                sf.send_pyobj("start")
            self.start_time = time.time()
            self.sig_cycles = 0
            self.sig2_cycles = 0
            self.f0 = 2840.
            self.f1 = 2940.
            self.f2 = self.f0
            self.df = (self.f1 - self.f0) / self.period * self.dtf
            self.speed = (self.f1 - self.f0) / self.period
            self.timer_freq.start(self.dt)
        else:
            if self.checkBox.isChecked():
                sf.send_pyobj("stop")
    def on_rb_stop_toggled(self,state):
        if state:
            self.stop()
    def stop(self):
        print("stop")
        self.timer_freq.stop()
        self.timer_field.stop()
        self.timer_wl.stop()
    def on_btn_sendrange_pressed(self):
        sfld.send_pyobj(("range",(np.random.random()*0.5,np.random.random()*.5+.5)))
    def on_rb_field_toggled(self,state):
        if state:
            self.stop()
            print("field")
            if self.releaseAfterUse:
                sfld.bind("tcp://127.0.0.1:{}".format(sfld_port))
            if self.checkBox.isChecked():
                time.sleep(1)
                for i in range(0,10):
                    sfld.send_pyobj(i)
                sfld.send_pyobj("start")
            self.start_time = time.time()
            self.sig_cycles = 0
            self.sig2_cycles = 0
            self.f0 = 0
            self.f1 = 1
            self.df = .01
            self.f2 = self.f0
            #self.df = (self.f1 - self.f0) / self.period * self.dtf
            self.speed = (self.f1 - self.f0) / self.period
            # !!!! self.timer_field.start(dt)
            self.timer_field.start(self.dt)
        else:
            if self.checkBox.isChecked():
                sfld.send_pyobj("stop")
            if self.releaseAfterUse:
                sfld.unbind("tcp://127.0.0.1:{}".format(sfld_port))
    def on_rb_wl_toggled(self,state):
        if state:
            self.stop()
            print("wavelength")
            swl.bind("tcp://127.0.0.1:{}".format(swl_port))
            time.sleep(1)
            self.start_time = time.time()
            if self.checkBox.isChecked():
                swl.send_pyobj("start")
            self.sig_cycles = 0
            self.sig2_cycles = 0
            self.f0 = 800.e-9
            self.f1 = 950.e-9
            self.f2 = self.f0
            self.df = (self.f1 - self.f0) / self.period * self.dtf
            self.speed = (self.f1 - self.f0) / self.period
            self.timer_wl.start(self.dt)
        else:
            if self.checkBox.isChecked():
                swl.send_pyobj("stop")
            swl.unbind("tcp://127.0.0.1:{}".format(swl_port))
    def freq(self):
        self.sim(1)
    def sim(self,mode):
        ch1 = (np.random.random() - 0.5) * .01
        ch2 = (np.random.random() - 0.5) * .01
        t = time.time()
        elapsed = t - self.start_time
        elapsed_in_this_period = elapsed - int(elapsed) / self.period * self.period
        steps = int(elapsed_in_this_period / self.dtf)
        f = self.f0 + steps * self.df

        if f != self.f2:
            if mode==1:
                sf.send_pyobj((t, f*1e6))
            elif mode==self.MODE_FIELD:
                sfld.send_pyobj((t, f))
            elif mode==3:
                swl.send_pyobj((t, f))
        self.f2 = f
        #print time.time()-t
        if int(elapsed / self.dts) > self.sig_cycles:
            x = self.f0 + elapsed_in_this_period * speed
            if mode==1:
                sig = simNVfreq(x)+ch1
            elif mode==2:
                sig = simField(x)+ch1
            elif mode==3:
                sig = simWl(x)#+ch1
            self.sig_cycles += 1
            ss.send_pyobj((t, sig, ch2))
            #print self.sig_cycles,self.sig2_cycles
        if int(elapsed / self.dts2) > self.sig2_cycles:
            x = self.f0 + elapsed_in_this_period * speed
            if mode==1:
                sig = simNVfreq(x)+ch1
            elif mode==2:
                sig = simField(x)+ch1
            elif mode==3:
                sig = simWl(x)+ch1
            self.sig2_cycles += 1
            ss2.send_pyobj((t, sig, ch2))
            #print self.sig_cycles,self.sig2_cycles
            ##    msg = (i,elapsed, f, sig)
        #print time.time()-t
    def field(self):
        self.sim(2)
    def wl(self):
        self.sim(3)
        pass


opt = Options()
qapp = QtWidgets.QApplication([])
w = MainWnd()
def save_win_position(pos):
    opt['Window.geometry'] = pos
    opt.savetodisk

if 'Window.geometry' in opt:
    w.setGeometry(*opt['Window.geometry'])
w.GeometryChangedSig.connect(save_win_position)

w.show()
qapp.exec_()


sf.close()
ss.close()
context.destroy()
