from __future__ import print_function
__author__ = 'user'
import pyqtgraph as pg

from TabManager import TabManager
import traceback
# class Cursor(pg.QtCore.QObject):
#     def __init__(self):
#         super(Cursor,self).__init__()
#
#     def setPosition(self,x,y):
#         pass
class Cursor(pg.GraphicsObject):
    x = 1000
    y = 1000
    def __init__(self, *data):
        pg.GraphicsObject.__init__(self)
        self.data = [100,100]  ## data must have fields: time, open, close, min, max
        self.size = 1000
        self.generatePicture()
        pg.ArrowItem()
        pg.TextItem()

    def generatePicture(self):
        ## pre-computing a QPicture object allows paint() to run much more quickly,
        ## rather than re-drawing the shapes every time.
        self.picture = pg.QtGui.QPicture()
        p = pg.QtGui.QPainter(self.picture)
        p.setPen(pg.mkPen('w'))
        hw = self.size/2
        # w = (self.data[1][0] - self.data[0][0]) / 3.
        # for (t, open, close, min, max) in self.data:
        p.drawLine(pg.QtCore.QPoint(hw,0 ), pg.QtCore.QPoint(hw, self.size))
        p.drawLine(pg.QtCore.QPoint(0,hw ), pg.QtCore.QPoint(self.size, hw))
            # if open > close:
            #     p.setBrush(pg.mkBrush('r'))
            # else:
            #     p.setBrush(pg.mkBrush('g'))
            # p.drawRect(pg.QtCore.QRectF(t-w, open, w*2, close-open))
        p.end()
##        self.p = p
    def setPosition(self,x,y):
        self.x = x
        self.y = y
        p = pg.QtGui.QPainter(self.picture)
        p.drawPicture(self.x,self.y,self.picture)
        p.end()
    def paint(self,p,*arg):
        p.drawPicture(self.x, self.y, self.picture)

    def boundingRect(self):
        ## boundingRect _must_ indicate the entire area that will be drawn on
        ## or else we will get artifacts and possibly crashing.
        ## (in this case, QPicture does all the work of computing the bouning rect for us)
        return pg.QtCore.QRectF(self.picture.boundingRect())

class CurrentPointCursor(pg.QtCore.QObject):
    def __init__(self,source_list,tab_manager):
        super(CurrentPointCursor,self).__init__()
        self.sources_list = source_list
        self.tab_manager = tab_manager
        self.update_src_list()
    def update_src_list(self):
        self.data = [[] for src in self.sources_list]
    def setPoint(self,arg_tuple):
        try:
            #print arg_tuple
            srcN = arg_tuple[0]
            self.data[srcN] = list(arg_tuple)[1:]
        except:
            traceback.print_exc()
        self.showCursor()
    def showCursor(self):
        for srcN,pm in self.tab_manager.tabs_dict.items():
            if pm.mode!=None:
                print(pm.plot_dict_list[pm.mode])

class CrossHair(object):
    def __init__(self,plot):
        # cross hair
        self.vLine = pg.InfiniteLine(angle=90, movable=False)
        self.hLine = pg.InfiniteLine(angle=0, movable=False)
        plot.addItem(self.vLine, ignoreBounds=True)
        plot.addItem(self.hLine, ignoreBounds=True)
    def setPos(self,x,y):
        self.vLine.setPos(x)
        self.hLine.setPos(y)

if __name__ == '__main__':
    import sys
    from numpy.random import random
    #generate layout
    app = pg.QtGui.QApplication([])
    win = pg.GraphicsWindow()
    win.setWindowTitle('pyqtgraph example: crosshair')
    label = pg.LabelItem(justify='right')
    win.addItem(label)
    p1 = win.addPlot(row=1, col=0)
    p2 = win.addPlot(row=2, col=0)

##    region = pg.LinearRegionItem()
##    region.setZValue(10)
    # Add the LinearRegionItem to the ViewBox, but tell the ViewBox to exclude this
    # item when doing auto-range calculations.
##    p2.addItem(region, ignoreBounds=True)

    #pg.dbg()
    p1.setAutoVisible(y=True)


    #create numpy arrays
    #make the numbers large to show that the xrange shows data from 10000 to all the way 0
    data1 = 10000 + 15000 * pg.gaussianFilter(random(size=10000), 10) + 3000 * random(size=10000)
    data2 = 15000 + 15000 * pg.gaussianFilter(random(size=10000), 10) + 3000 * random(size=10000)

    p1.plot(data1, pen="r")
    p1.plot(data2, pen="g")

    p2.plot(data1, pen="w")
    c = Cursor()
    p1.addItem(c)
    ch = CrossHair(p2)
    def update():
        x = random()*100
        y = random()*100
        c = Cursor()
        c.setPosition(x,y)
        p1.addItem(c)
        print(x,y)
##    region.sigRegionChanged.connect(update)





    # r = Cursor()
    # r.addPlot(p1,p2)
    # r.setRange(1100,1800)
    # r.setMovable(True)
    # r.addPlot(p2)
    # r.setVisible(True)
##    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
    tm= pg.QtCore.QTimer()
    tm.timeout.connect(update)
    tm.start(100)
    pg.QtGui.QApplication.instance().exec_()