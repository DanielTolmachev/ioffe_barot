import sys,logging
sys._excepthook = sys.excepthook
logging.basicConfig(filename='exceptions.log',filemode="w", level=logging.DEBUG)
def exception_hook(exctype, value, trace_back):
    logging.exception("Exception",exc_info=(exctype, value, trace_back),stack_info = False)
    sys._excepthook(exctype, value, trace_back)
sys.excepthook = exception_hook