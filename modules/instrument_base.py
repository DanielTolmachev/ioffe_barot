#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      user
#
# Created:     08.11.2015
# Copyright:   (c) user 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from __future__ import print_function
class InstrumentBase(object):
    connected = False
    keep_connection = False
    def __init__(self,dev_address,**kwargs):
        """
        dev_address: addres of device
        """
        self.setupInterface(dev_address,**kwargs)
    def dummy_read(self):
        print(self,"dummy read")
    def dummy_write(self,data):
        print(self,"write:",data)
    def ask(self,req):
        self.write(req)
        return self.read()
    def read(self):
        return self.dummy_read()
    def write(self,data):
        self.dummy_write(data)
    def open(self):
        pass
    def close(self):
        pass
    def isOpen(self):
        return self.interface.connected
    def isConnected(self): #syn
        return self.isOpen()
    def setDeviceAddress(self, device_address):
        self.setupInterface(device_address)
    def getDeviceAddress(self):
        return self.address
    def setupInterface(self,address,**kwargs):
        self.address = address
        if type(address)==tuple and len(address)==2\
                and type(address[1])==int \
                and 0<address[1]<65535 \
                and type(address[0])==str:
            if address[0].lower().startswith("com"):
                print("device address is", address[0], " (serial port)")
                self.address = address[0]
                self.useSerial(self.address)
                return
            else:
                print("device address is",address," (Ethernet)")
                self.useEthernet(address,**kwargs)
                return
        elif type(address)==str and address.lower().startswith("com"):
            print("device address is", address, " (serial port)")
            self.useSerial(address,**kwargs)
            return
        print("InstrumentBase: unnown address type",address)
    def useEthernet(self,address, **kwargs):
        from socketA import SocketA
        self.interface = SocketA(address, **kwargs)
        self.read = self.interface.read
        self.write = self.interface.write
        self.open = self.interface.open
        self.close = self.interface.close
    def useSerial(self,address,**kwargs):
        from serialA import SerialA
        self.interface = SerialA(address,**kwargs)
        self.read = self.interface.readline
        self.write = self.interface.write
        self.open = self.interface.open
        self.close = self.interface.close
    def getID(self):
        rep = self.ask("*IDN?\r\n")
        if rep:
            self.idn = rep
        return rep

