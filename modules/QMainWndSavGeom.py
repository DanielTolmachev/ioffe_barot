from __future__ import print_function,unicode_literals
from qtpy import QtCore,QtWidgets
class QMainWindowAutoSaveGeometry(QtWidgets.QMainWindow):
    """
        this class implements method for saving window geometry in 3 ways:
1. GeometryChangedSig is emitted after 5 sec after resize or move
2. saveGeometry is called (you can reimplement this method)
3. callback is called (you can set it using setSaveGeometryCallback)
    """
    GeometryChangedSig = QtCore.Signal(object)
    GeometryChangedCallback=None
    GeometryChangedTimeOut = 5000
    _GeometryChangedFlag = False
    def __init__(self,QuitOnClose = True):
        super(QMainWindowAutoSaveGeometry,self).__init__()
        self._GeometryChangedTimer = QtCore.QTimer()
        self._GeometryChangedTimer.setSingleShot(True)
        self._GeometryChangedTimer.stop()
        self._GeometryChangedTimer.timeout.connect(self.__geometryChanged)
        self.quit_on_close = QuitOnClose
    def __geometryChanged(self):
        g = self.geometry()
        #print g
        g = (g.left(),g.top(),g.width(),g.height())
        print("Window \"{}\" geometry changed {}".format(self.windowTitle(),g))
        self._GeometryChangedFlag = False
        self.GeometryChangedSig.emit(g)
        self.saveGeometry()
        if self.GeometryChangedCallback:
            self.GeometryChangedCallback(g)
    def resizeEvent(self,*args,**kwargs):
        self._GeometryChangedFlag = True
        self._GeometryChangedTimer.start(5000)
    def moveEvent(self,*args,**kwargs):
        self._GeometryChangedFlag = True
        self._GeometryChangedTimer.start(5000)
    def closeEvent(self, *args, **kwargs):
        if self._GeometryChangedFlag:
            self._GeometryChangedTimer.stop()
            self.__geometryChanged()
        if self.quit_on_close:
            QtWidgets.QApplication.closeAllWindows()
    def setSaveGeometryCallback(self,callback):
        self.GeometryChangedCallback = callback
    def setGeometryChangedTimeOut(self,val):
        self.GeometryChangedTimeOut = val
    def loadGeometry(self,geometry):
        if type(geometry)==tuple:
            if len(geometry)==4:
                (left,top,width,height) = geometry
        elif type(geometry)== QtCore.QRect:
            (left,top,width,height) = (geometry.left(),geometry.top(),geometry.width(),geometry.height())
        else:
            print('incorrect window size')
            return
        scr_cnt = QtWidgets.QDesktopWidget().screenCount()
        scr_left = []
        scr_top = []
        for i in range(0,scr_cnt):
            scr_left.append(QtWidgets.QDesktopWidget().availableGeometry(i).left())
            scr_top.append(QtWidgets.QDesktopWidget().availableGeometry(i).top())
        scr_left = min(scr_left)
        scr_top = min(scr_top)

        screen_width = QtWidgets.QDesktopWidget().availableGeometry().width()
        screen_height = QtWidgets.QDesktopWidget().availableGeometry().height()
        if left< scr_left:
            left = scr_left




        elif left > screen_width-width:
            left = screen_width-width
        if top< scr_top:
            top = scr_top
        elif top > screen_height-height:
            top = screen_height-height
        if width< self.minimumWidth() or width > screen_width:
            width = self.width()
        if height< self.minimumHeight() or height > screen_height:
            height = self.height()
        self.setGeometry(left,top,width,height)
        # self.move ( xpos, ypos )
    def saveGeometry(self):
        pass