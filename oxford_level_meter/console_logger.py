# -*- coding: utf-8 -*-
__author__ = r"Daniel Tolmachev (Daniel.Tolmachev@mail.ioffe.ru/Daniel.Tolmachev@gmail.com)"

import sys,time,os,mmap,random
sys.path.append("../modules")
import install_exception_hook
import ILM_device_class
import logger
import roundto
try:
    import zmq_publisher
    pub = zmq_publisher.Zmq_Publisher()
except ImportError:
    pub = None




DEV_ADDRESS = "com5"


mmap_file = mmap.mmap(-1,1024,tagname="oxford_cryogen_level_logger",access=mmap.ACCESS_WRITE)
line = mmap_file.readline().strip(b"\0")
if line:
    lines = line.split()
    pid = port = None
    for l in lines:
        try:
            if b"PID" in l:
                pid = int(l[3:])
            elif b"PORT" in l:
                port = int(l[4:])
        except:
            pass
    if pid and port:
        print("process {} is already running at port {}".format(pid,port))
    else:
        print("process is already running {}".format(line))
    sys.exit(0)
else:
    mmap_file.seek(0)
    mmap_file.write(b"PID%d" % os.getpid())
    if pub:
        port = pub.bindRandomPort()
        print("zmq publisher (ZMQ.PUB) works on port %d"%port)
    mmap_file.write(b" PORT%d" % port)




helium_log = logger.Logger(fileprefix="helium",
                     path= ".",
                       header="#time\tHelium level (%)",
                       time_relative = False,
                       flush_immidiately = True,
                       testmode = False,
                       precision = 0.1
                       )

nitrogen_log = logger.Logger(fileprefix="nitrogen",
                     path= ".",
                       header="#time\tNitrogen level (%)",
                       time_relative = False,
                       flush_immidiately = True,
                       testmode = False,
                       precision = 0.01
                       )



ilm = ILM_device_class.ILM(DEV_ADDRESS)
ilm.open()


class Level(object):
    newline = '\r\n'
    def __init__(self,logger,threshold = 1,time_resolution = 10, filling_log_name = None):
        self.logger = logger
        self.prev_val = None
        self.prev_time = None
        self.time_sum = None
        self.prev_time0 = None
        self.threshold = threshold
        self.time_resolution = time_resolution
        self.filling_start_val = None
        self.filling_start_time = None
        self.filling_in_progress = False
        self.filling_log_name = filling_log_name
        self.filling_log = None
    def addValue(self,val,t = None):
        if not t:
            t = time.time()
        if self.prev_val is None:
            self.logger.appendData( val,time = t)
            self.prev_val = val
            self.val_cnt = 1
            self.time_sum = self.prev_time0 = t
        else:
            if val==self.prev_val:
                self.val_cnt += 1
                self.time_sum += t
                if self.filling_in_progress and t-self.prev_time0>self.time_resolution:
                    self._fillingStopped(val)
            elif type(val) == float and val!=self.prev_val:
                if val>self.prev_val:
                    if self.filling_start_val is None:
                        self.filling_start_val = self.prev_val
                        self.filling_start_time = self.prev_time
                    if val>=self.filling_start_val+self.threshold:
                        self.filling_in_progress = True
                        self._fillingStarted()
                elif val<self.prev_val and self.filling_in_progress:
                    self._fillingStopped(val)
                tav = self.time_sum/self.val_cnt
                self.logger.appendData(self.prev_val,time = tav)
                self.prev_val = val
                self.val_cnt = 1
                self.time_sum = self.prev_time0 = t
        self.prev_time = t
    def _fillingStarted(self):
        if not self.filling_log:
            if self.filling_log_name:
                if os.path.exists(self.filling_log_name):
                    newfile = False
                else:
                    newfile = True
                self.filling_log = open(self.filling_log_name,"a+",newline= self.newline)
                if newfile:
                    self.filling_log.write("date\tstart time\tstarting level\tend time\tend level"+self.newline)
        if self.filling_log:
             ts = time.localtime(self.filling_start_time)
             msg = time.strftime("%Y-%m-%d\t%H:%M:%S\t",ts)+str(self.filling_start_val)+self.newline
             self.filling_log.write(msg)
             self.filling_log.flush()
        
    def _fillingStopped(self,val):
        if self.filling_log:
            self.filling_log.seek(self.filling_log.tell()-len(self.newline))
            msg = time.strftime("\t%H:%M:%S\t",time.localtime())+str(val)+"\n"
            self.filling_log.write(msg)
            self.filling_log.flush()
        self.filling_start_val = None
        self.filling_start_time = None
        self.filling_in_progress = False
                
helium = Level(helium_log,filling_log_name="helium_filling_log.txt")
nitrogen = Level(nitrogen_log,filling_log_name="nitrogen_filling_log.txt",threshold=5)   
     
def leveling_main(ilm):    
    t = time.time()    
    lhe = ilm.helium()    
    ln2 = ilm.nitrogen()
    helium.addValue(lhe)
    nitrogen.addValue(ln2)
    if pub:
        pub.send_pyobj((lhe,ln2))
    return lhe,ln2



while True:
    lhe,ln2 = leveling_main(ilm)
    print("levels are: {}  {}\r".format(lhe,ln2))
    time.sleep(1)
    

