#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Daniel
#
# Created:     14.08.2014
# Copyright:   (c) Daniel 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import pyqtgraph as pg
import numpy as np


x = np.arange(10)
y = np.arange(10) %3
er = np.arange(0.1,1.1,0.1)
top = np.linspace(1.0, 3.0, 10)
bottom = np.linspace(2, 0.5, 10)

plt = pg.plot()
plt.setWindowTitle('pyqtgraph example: ErrorBarItem')
err = pg.ErrorBarItem(x=x, y=y, height = er,top=top, bottom=bottom, beam=0.5)
plt.addItem(err)
#plt.plot(x, y, symbol='o', pen={'color': 0.0, 'width': 2})
plt.plot(x, y, symbol='o', pen=None)
err = pg.ErrorBarItem(x=x, y=y, height = 1,top=top, bottom=bottom, beam=0.5)
## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
        pg.QtGui.QApplication.instance().exec_()
