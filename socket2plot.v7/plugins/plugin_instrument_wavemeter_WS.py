import time

from plugins.instr_plugin_base import Instr_Plugin_Base
import ws7_class



DEFAULT_TIMEOUT = 0.1


class WS_wavelength(Instr_Plugin_Base):
    __instrument_info__ = {
    "channels2rec" : 1,
    "timechannel" : 0,
    "name" : 'WS7 Wavelength',
    "comment" : 'Wavelengthmeter WS7 by High Finesse',
    "channels_names" : ['Time',"Wavelength"],
    "channels_units" : ['s',"nm"]}

    def __init__(self,ID,settings = {}):
        super(WS_wavelength,self).__init__(ID,settings)
        self.loadattr("timeout",DEFAULT_TIMEOUT)
        self.dev = ws7_class.Wavemeter()
    def run(self):
        while self._running:
            wl = self.dev.getWaveLength()
            # lw = self.dev.getLineWidth_nm
            if wl>0:
                wl = round(wl,6)
                self.updated.emit((self._id,time.time(),wl))
            time.sleep(self.timeout)


class WS_linewidth_nm(Instr_Plugin_Base):
    __instrument_info__ = {
    "channels2rec" : 1,
    "timechannel" : 0,
    "name" : 'WS7 LineWidth',
    "comment" : 'Wavelengthmeter WS7 by High Finesse',
    "channels_names" : ['time',"Linewidth"],
    "channels_units" : ['s',"nm"]}
    def __init__(self,ID,settings = {}):
        super(WS_linewidth_nm,self).__init__(ID,settings)
        self.loadattr("timeout",DEFAULT_TIMEOUT)
        self.dev = ws7_class.Wavemeter()
    def run(self):
        while self.started:
            # wl = self.dev.getWaveLength()
            lw = self.dev.getLineWidth_nm()
            if lw>0:
                self.updated.emit((self._id,time.time(),lw))
            time.sleep(self.timeout)
    def stop(self):
        self.started = False


__pluginclass__ = [WS_wavelength,WS_linewidth_nm]

