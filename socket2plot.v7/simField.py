#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      user
#
# Created:     16.11.2015
# Copyright:   (c) user 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import pyqtgraph as pg
import numpy as np
def simField(x):
    return np.exp(-(x - 810e-9) ** 2 / (2e-9)**2) + np.exp(-(x - 840e-9) ** 2 / 15e-9**2)+\
np.exp(-(x - 870e-9) ** 2 / 20e-9**2)*.8

def main():
    pass


if __name__ == '__main__':
    x = np.linspace(800e-9,950e-9,1000)
    y = np.zeros(1000)
    for i in xrange(0,1000):
        y[i] = simField(x[i])
    pg.plot(x,y)