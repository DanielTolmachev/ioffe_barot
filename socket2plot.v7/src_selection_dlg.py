#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Daniel
#
# Created:     16.02.2015
# Copyright:   (c) Daniel 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from __future__ import print_function
from qtpy import QtCore, QtWidgets
import sys
from sources import list_of_sources
from options import Options


class Src_Selection_Dlg(QtWidgets.QDialog):
    accept = QtCore.Signal()
    exclude_time_channel = True
    def __init__(self,srcl):
        self.srcl = srcl
        opt = Options()
        self.hideDisabledControls = opt.get("gui.Source_dlg.HideDisabledControls",True)
        super(Src_Selection_Dlg, self).__init__()
        self.setWindowTitle("Select signal source")
        self.bb = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.StandardButtons(
            QtWidgets.QDialogButtonBox.Apply +
            QtWidgets.QDialogButtonBox.Ok +
            QtWidgets.QDialogButtonBox.Cancel))
        self.bb.button(self.bb.Ok).clicked.connect(self.dlg_accepted)
        self.bb.button(self.bb.Apply).clicked.connect(self.dlg_apply)
        self.bb.button(self.bb.Cancel).clicked.connect(self.dlg_close)
        self.vl = QtWidgets.QGridLayout()
        self.vl.addWidget(QtWidgets.QLabel('source'),0,0)
        self.vl.addWidget(QtWidgets.QLabel('channel\nname'), 0, 1)
        self.vl.addWidget(QtWidgets.QLabel('param'), 0, 2)
        self.vl.addWidget(QtWidgets.QLabel('record\nchannel'),0,3)
        self.vl.addWidget(QtWidgets.QLabel('display\nchannels'),0,4)
        self.vl.addWidget(QtWidgets.QLabel('X'),0,5)
        self.setLayout(self.vl)
        self.cb_source = [] #checkboxes list
        self.cb_chans2rec = [] #checkboxes channels list
        self.cb_chans_params = []  # checkboxes channels list
        self.cb_chans2show = [] #checkboxes for channels that should be displayed
        self.chans = [] # channels list
        self.channels_count_list = []
        self.channels_list = []
        cnt = 1
        xa = -1
        for i,src in enumerate(srcl):
            self.channels_list.append(list(range(src.channels_count)))
            if self.exclude_time_channel and hasattr(src,'timechannel'):
                self.channels_count_list.append(src.channels_count-1)
                self.channels_list[i].pop(self.channels_list[i].index(src.timechannel))
            else:
                self.channels_count_list.append(src.channels_count)

            print("dlg_init:",src.name,self.channels_list[i],src.channels2rec,src.channels2show)
            cb_rec = QtWidgets.QCheckBox(src.name)
            if hasattr(src,"comment"):
                cb_rec.setToolTip(src.comment)
            cb_rec.setChecked(src.enabled)
            cb_rec.stateChanged.connect(self.src_checked)
            self.cb_source.append(cb_rec)
            self.vl.addWidget(cb_rec,cnt,0)
            self.chans.append(i)
            for c in range(0,src.channels_count):
                if not self.exclude_time_channel or (hasattr(src,'timechannel') and c!=src.timechannel):
                    if hasattr(src,'channels_names'):
                        n = src.channels_names[c]
                    else:
                        n = str(c)
                    if type(n)==list:
                        print(src.name)
                        print(src.channels2rec)
                    chan_label = QtWidgets.QLabel(n)
                    cb_rec = QtWidgets.QCheckBox()
                    cb_rec.toggled.connect(self.toggleParamRecord)
                    if hasattr(src,"channels_units") and len(src.channels_units)<=c:
                        cb_rec.setToolTip(src.channels_units[c])
                    cb_show = QtWidgets.QCheckBox()
                    cb_par = QtWidgets.QCheckBox()
                    cb_par.toggled.connect(self.toggleParamRecord)
                    if src.xaxis!=None:  #xaxis can be zero
                        if src.xaxis==c:
                            if xa<0:
                                print('X asxis {} source: {}'.format(c,src))
                                xa = cnt
                            else:
                                print('X asxis {} source: {}\tignored'.format(c,src))
                    if c in src.channels2rec:
                        cb_rec.setChecked(True)
                    if c in src.channels2show:
                        cb_show.setChecked(True)
                    if c in src.channels2params and c not in src.channels2rec:
                        cb_par.setChecked(True)
                    cb_par.setEnabled(src.enabled)
                    cb_rec.setEnabled(src.enabled)
                    cb_show.setEnabled(src.enabled)
                    if self.hideDisabledControls:
                        cb_par.setVisible(src.enabled)
                        cb_rec.setVisible(src.enabled)
                        cb_show.setVisible(src.enabled)
                    self.vl.addWidget(chan_label, cnt, 1)
                    self.vl.addWidget(cb_par, cnt, 2)
                    self.vl.addWidget(cb_rec,cnt,3)
                    self.vl.addWidget(cb_show,cnt,4)
                    self.cb_chans2rec.append(cb_rec)
                    self.cb_chans_params.append(cb_par)
                    self.cb_chans2show.append(cb_show)
                    cnt+=1


        self.vl.addWidget(self.bb,cnt,0,1,5)
        self.rbgroup = QtWidgets.QButtonGroup()
        for i in range(1,cnt):
            rb = QtWidgets.QRadioButton()
            if i==xa:
                rb.setChecked(True)
            rb.setEnabled(self.cb_chans2rec[i - 1].isEnabled())
            if self.hideDisabledControls:
                rb.setVisible(self.cb_chans2rec[i - 1].isEnabled())
            self.vl.addWidget(rb,i,5)
            self.rbgroup.addButton(rb)
    def checkbox2src(self,cb):
        for i,c in enumerate(self.cb_source):
            if cb==c:
                return i
        return -1
    def find_chan_idx(self,srcN):
        cnt = cnt2 = 0
        for i,src in enumerate(self.srcl):
            if i==srcN:
                cnt2 = cnt+self.channels_count_list[i]
                break
            cnt +=self.channels_count_list[i]
        print(cnt,cnt2)
        return cnt,cnt2
    def src_checked(self,evt):
        cb = self.sender()
        srcN = self.checkbox2src(cb)
        i1,i2 = self.find_chan_idx(srcN)
        status = bool(evt)
        for i in range(i1,i2):
            self.cb_chans2rec[i].setEnabled(status)
            self.cb_chans2show[i].setEnabled(status)
            self.cb_chans_params[i].setEnabled(status)
            self.rbgroup.buttons()[i].setEnabled(status)
            if status or self.hideDisabledControls: #hide if needed, always show
                self.cb_chans2rec[i].setVisible(status)
                self.cb_chans2show[i].setVisible(status)
                self.cb_chans_params[i].setVisible(status)
                self.rbgroup.buttons()[i].setVisible(status)

    def dlg_accepted(self):
        self.dlg_close()
        self.dlg_apply()

    def dlg_close(self):
        self.hide()

    def dlg_apply(self):
        cnt = 0
        #xb = self.bg.checkedId()
        #print xb
        for srcN,(src,cb) in enumerate(zip(self.srcl, self.cb_source)):
            src.enabled = cb.isChecked()
            if src.xaxis:
                src._xaxis = src.xaxis
            src.xaxis = None
            #print cnt, cnt+len(src.channels2rec)
            src.channels2rec = []
            src.channels2show = []
            src.channels2params = []
            #src.xaxis = None
            for i in range(0,self.channels_count_list[srcN]):
                cb = self.cb_chans2rec[i + cnt]
                cb2 = self.cb_chans2show[i+cnt]
                cb_par = self.cb_chans_params[i+cnt]
                chan = self.channels_list[srcN][i]
                if cb.isChecked():
                    src.channels2rec.append(chan)
                if cb2.isChecked(): #check if we need to show channel
                    src.channels2show.append(chan)
                if cb_par.isChecked() and not cb.isChecked():
                    src.channels2params.append(chan)
                if self.rbgroup.buttons()[i].isChecked():
                    src.xaxis = chan
            cnt+= self.channels_count_list[srcN]
            print("dlg_apply:",src.name,src.channels2rec,src.channels2show)


        self.srcl.savestate()
        self.accept.emit()
##    def src_checked(self,state):
##        print state
##        sndr = QtCore.QObject.sender(self)
        #self.srcl.printout()
    def toggleParamRecord(self,state):
        sndr = self.sender()
        if sndr in self.cb_chans2rec:
            idx = self.cb_chans2rec.index(sndr)
            self.cb_chans_params[idx].setChecked(not state)
        elif sndr in self.cb_chans_params:
            idx = self.cb_chans_params.index(sndr)
            self.cb_chans2rec[idx].setChecked(not state)

if __name__ == '__main__':
    qapp = QtWidgets.QApplication(sys.argv)
    sd = Src_Selection_Dlg(list_of_sources)
    sys.exit(sd.exec_())
