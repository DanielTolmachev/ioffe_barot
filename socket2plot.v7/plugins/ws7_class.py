#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      C7LabPC
#
# Created:     12.08.2016
# Copyright:   (c) C7LabPC 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from __future__ import print_function, unicode_literals
from ctypes import WinDLL, c_double,c_long,c_uint8
from wlm_constants import *
import time

class Wavemeter(WinDLL):
    errors = {}
    dummyd = c_double(0)
    def __init__(self):
        super(Wavemeter,self).__init__("wlmData.dll")
        self.lasterror = ""
        self.lastfunc = ""
##        self.getOperationState = self.GetOperationState
##        self.getOperationState.restype = c_uint8
        self.GetWavelength.restype = c_double
        self.GetLinewidth.restype = c_double
    def call(self,func_name,*args):
        func = self.__getattr__(func_name)
        ret = func(*args)
        self.lasterror = ret
        self.lastfunc = func_name
        #self.getError(ret)
        return ret
    def calld(self,func_name,*args):
        func = self.__getattr__(func_name)
        func.restype = c_double
        ret = func(*args)
        self.lasterror = ret
        self.lastfunc = func_name
        #self.getError(ret)
        return ret
    def getError(self,err,pattern = "Err"):
        for key,val in globals().iteritems():
            if val==err and key.startswith(pattern):
                #print("error: {} ({})".format(key,val))
                return key
    def showLastError(self):
        pattern = ""
        if self.lastfunc.startswith("GetFrequency"):
            pattern = "Err"
        elif self.lastfunc.startswith("Set") or self.lastfunc=="Operation":
            pattern = "ResERR"
        elif self.lastfunc.startswith("Control"):
            pattern = "fl"
        if pattern:
            err_str = self.getError(self.lasterror,pattern)
            if err_str:
                print("{} returns {} ({})".format(self.lastfunc,err_str,self.lasterror))
                return
        print("{} returns {}".format(self.lastfunc,self.lasterror))
        return
    def start(self):
        if not wm.call("Instantiate",0,0,0,0):
            wm.call("ControlWLMEx",cCtrlWLMHide|cCtrlWLMSilent|cCtrlWLMWait,0,0,1,0)
        if self.isRunning != cMeasurement:
            wm.call("Operation",cCtrlStartMeasurement)
    def isRunning(self):
        return c_uint8(self.GetOperationState(0))
    def getWaveLength(self):
        return self.GetWavelength(self.dummyd)
    def getLineWidth_nm(self):
        return self.GetLinewidth(cReturnWavelengthVac,self.dummyd)






if __name__ == '__main__':
    wm = Wavemeter()
##    wm.call("ControlWLM",cCtrlWLMHide|cCtrlWLMSilent,0,0)
##    while not wm.call("Instantiate",0,0,0,0):
##        time.sleep(1)
##    wm.call("Operation",cCtrlStartMeasurement)
    #wm.start()
    wm.showLastError()
    print(  wm.calld("GetWavelength",c_double(0)),
            wm.calld("GetLinewidth",cReturnWavelengthVac,c_double(0)))
    print(wm.getWaveLength())