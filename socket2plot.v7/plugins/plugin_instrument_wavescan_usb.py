from qtpy import QtCore
import time,socket,traceback
import ape_device

DEVICE_PORT = 51123
DEVICE_IP = "127.0.0.1"
DEFAULT_SOCKET_TIMEOUT = 0.1

class Wavescan(QtCore.QThread):
    __instrument_info__ = {
    "channels2rec" : 1,
    "timechannel" : 0,
    "name" : 'Wavescan_port_51123',
    "comment" : 'Wavescan USB spectrometer',
    "channels_names" : ['time',"Wavelength"],
    "channels_units" : ['s',"nm"]}

    updated = QtCore.Signal(tuple)
    _id = -1


    DEFAULT_REPETITION_RATE = 0.1
    DEFAULT_RECONNECT_TIME = 1
    def __init__(self,ID,settings = {}):
        super(Wavescan,self).__init__()
        self._id = ID
        self.started = True
        self.name = self.__instrument_info__.get("name","")
        self.settings = settings
        self.device_port = DEVICE_PORT
        self.device_ip = DEVICE_IP
        self.loadattr("timeout",self.DEFAULT_REPETITION_RATE)
        self.loadattr("ip",DEVICE_IP)
        self.loadattr("port",DEVICE_PORT)
        self.loadattr("sock_timeout",DEFAULT_SOCKET_TIMEOUT)
        self.dev = ape_device.ape_device(self.ip, self.port)
    def loadattr(self,attrname,defaults):
        setsname = self.name+"."+ attrname
        if setsname in self.settings:
            setattr(self,attrname,self.settings[setsname])
        else:
            setattr(self,attrname,defaults)
            self.settings[setsname] = defaults
    def run(self):
        while self.started:
            while not self.dev.connected:
                try:
                    #print("trying to connect to {}:{}".format(self.ip,self.port))
                    # self.dev.connect()
                    self.dev.dev = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    self.dev.dev.settimeout(self.sock_timeout)
                    #print("timeout:",self.sock_timeout,self.dev.dev.timeout)
                    self.dev.dev.connect((self.dev.host, self.dev.port))
                    self.dev.connected = True
                    print("connected to {}:{}".format(self.ip,self.port))
                    if not self.dev.connected:
                        time.sleep(self.DEFAULT_RECONNECT_TIME)
                except socket.timeout:
                    time.sleep(self.timeout)
                except socket.error:
                    traceback.print_exc()
                    pass
                except:
                    traceback.print_exc()
            try:
                # print("wavescan:sending request")

                t = time.time()
                peak0 = self.dev.query("SPECTRUM:PEAK?",False)
                self.updated.emit((self._id,t,peak0))
                #print ("wl peak",peak0)
                time.sleep(self.timeout)
            except socket.timeout:
                pass
                self.checkIfRuns()
            except socket.error:
                traceback.print_exc()
                self.dev.connected = False
            except:
                traceback.print_exc()
        print("wavescan plugin is stopped")
    def stop(self):
        print("wavescan is requested to stop")
        self.started = False
    def checkIfRuns(self):
        run = self.dev.query("STATUS:RUN?", False)
        # print("answer is",run)
        if run != "1":
            print("WaveScan USB is not running, starting")
            print((self.dev.send("STATUS:RUN=1")))



__pluginclass__ = Wavescan

